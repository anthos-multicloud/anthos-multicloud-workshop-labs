# AMCW: Online Boutique CI/CD Pipeline

## Objective

1. Deploy a microservice based application to multiple clusters in multiple cloud environments using a CI/CD pipeline.

## Setup

- In the GCP console, open [Cloud Shell](https://shell.cloud.google.com/). This lab is intended to be run from Cloud Shell.

- Wait for the `vars.sh` file to be available.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      gsutil ls gs://${DEVSHELL_PROJECT_ID}/vars.sh &> /dev/null
      exit_code=$?
      
      clear

      if [ ${exit_code} -eq 0 ]; then
          echo "${TIMESTAMP} vars.sh file is available"  
          break
      fi
      
      echo "${TIMESTAMP} vars.sh file is not ready yet..."  
      sleep 5
  done
  ```

- Create a `WORKDIR` for this tutorial. All files related to this tutorial end up in `WORKDIR`.

  ```bash
  mkdir -p $HOME/anthos-multicloud && cd $HOME/anthos-multicloud && export WORKDIR=$HOME/anthos-multicloud
  ```

- Clone the workshop repo.

  ```bash
  git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop.git ${WORKDIR}/anthos-multicloud-workshop
  ```

- Download the `var.sh` file,

  ```bash
  gsutil cp gs://${DEVSHELL_PROJECT_ID}/vars.sh ${WORKDIR}/
  ```

- Source the `vars.sh` file.

  ```bash
  source ${WORKDIR}/vars.sh
  ```

- Install the required tools.

  ```bash
  mkdir -p ~/.cloudshell && touch ~/.cloudshell/no-apt-get-warning
  ${WORKDIR}/anthos-multicloud-workshop/platform_admins/scripts/tools.sh
  ```

## User setup

- Check that all of the builds have completed successfully.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      ONGOING_BUILDS=`gcloud builds list --ongoing 2> /dev/null`
      ONGOING_BUILDS_COUNT=$(echo -n "${ONGOING_BUILDS}" | wc -l)

      clear

      if [ "${ONGOING_BUILDS_COUNT}" -eq "0" ]; then
          echo "${TIMESTAMP} All builds have completed, check the status of the builds at https://console.cloud.google.com/cloud-build/builds"
          break
      fi

      echo -en "${TIMESTAMP} ${ONGOING_BUILDS_COUNT} build(s) still running, build progress can also be monitored at https://console.cloud.google.com/cloud-build/builds\n${ONGOING_BUILDS}\n"
      sleep 5
  done
  ```

  <img alt="Image" src="img/cloudbuild_success.png" width=70% height=70%>

- Run the `user_setup.sh` script.

  ```bash
  source ${HOME}/.bashrc
  source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
  ```

- This `user_setup.sh` script performs the following steps:

  - Downloads EKS cluster _kubeconfig_ files. The location of these files is in the `${WORKDIR}/kubeconfig` folder.
  - Downloads SSH-Key pair. SSH Keys are used to interact with Gitlab repos. The location of these files is in the `${WORKDIR}/ssh-keys` folder.
  - Downloads the Gitlab hostname and root password txt file. The location of the file is in the `${WORKDIR}/gitlab` folder.
  - Creates a combined _kubeconfig_ file with all cluster contexts. Renames the clusters for easy context switching. The location of the merged _kubeconfig_ file is `${WORKDIR}/kubeconfig/workshop-config`. The script also sets this as your `KUBECONFIG` variable.
  - Get the EKS cluster's Kubernetes Service Account tokens to login to through the Cloud Console. Learn about logging in to Anthos registered clusters [here](https://cloud.google.com/anthos/multicluster-management/console/logging-in).
    > The script is idempotent and can be run multiple times.

  Example output from the end of the `user_setup.sh` script:

  ```bash output
  *** eks-prod-us-west2ab-1 Token ***

  [EKS Cluster Token]

  *** eks-prod-us-west2ab-2 Token ***

  [EKS Cluster Token]

  *** eks-stage-us-east1ab-1 Token ***

  [EKS Cluster Token]

  *** Gitlab Hostname and root password ***

  gitlab.endpoints.PROJECT_ID.cloud.goog
  [`root` PASSWORD]
  ```

### Variables

The `user_setup.sh` script adds additional variables for the workshop to the `vars.sh` file. The `vars.sh` file is automatically sourced when you log in to Cloud Shell. You can also manually source the file.

### Logging in to EKS clusters

There are three EKS clusters in the architecture. Two clusters in the `prod` environment and one in the `stage` environment. The tokens from the `user_setup.sh` script can be used to log in to the EKS clusters in Cloud Console.

- Navigate to the **Kubernetes Engine > Clusters** page in Cloud Console. You can see the three EKS clusters registered. They have not been logged in.

  <img alt="Image" src="img/eks_loggedout.png">

- Click **Login** next to each EKS cluster and select **Token**. Copy and paste the tokens (outputted from the `user_setup.sh` script) to the EKS clusters.
- Note: Simply select the token in the cloud shell (and nothing else, this automatically copies is), and paste it into the token box in the Cloud Console without intermittently storing it into a file. Any intermittent storage might add extra characters that will invalidate the token and login will fail.

  <img alt="Image" src="img/eks_login.png" width=40% height=40%>

- Navigate to the **Workloads** and **Services** pages and verify you can see metadata information from the EKS clusters. This confirms you have successfully logged in.

  <img alt="Image" src="img/eks_workloads.png" width=80% height=80%>

## Initialize repositories

1. Initialize the `shared-cd` repository, this repository contains the CI/CD jobs/stages required to deploy Online Boutique to the platform.
1. Initialize the `config` repository, this repository ensures that the Online Boutique namespaces are created in all clusters in the `prod` environment.
1. Initialize the `redis` repositorym this repository contains the CI/CD jobs/stages required to deploy a multicloud Redis cluster.

## shared-cd repository

The `shared-cd` repository contains Gitlab CI/CD [jobs](https://docs.gitlab.com/ee/ci/introduction/#how-gitlab-cicd-works) required to deploy applications to the Anthos multi-cloud platform. These jobs can be used in application repositories' CI/CD pipelines to deploy the application. In this lab, the `online-boutique` repo's CI/CD pipeline uses jobs defined in the `shared-cd` repo. This approach has the following benefits:

- Allows the platform admins to define CI/CD best practices to be shared by the application owners. Having a separate repository ensures proper access control.
- Allows consistent deployment methodology shared by all application teams. Applications do not have to maintain their own individual pipelines.
- Avoids code duplication (which increases chance of errors and mistakes).

1. Run the following commands to initialize the `shared-cd` repository.

   ```bash
   source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
   cd ${WORKDIR}
   # init git
   git config --global user.email "${GCLOUD_USER}"
   git config --global user.name "Cloud Shell"
   if [ ! -d ${HOME}/.ssh ]; then
     mkdir ${HOME}/.ssh
     chmod 700 ${HOME}/.ssh
   fi
   # pre-grab gitlab public key
   ssh-keyscan -t ecdsa-sha2-nistp256 -H gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog >> ~/.ssh/known_hosts
   git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:platform-admins/shared-cd.git
   cd ${WORKDIR}/shared-cd
   cp -r ${WORKDIR}/anthos-multicloud-workshop/platform_admins/starter_repos/shared_cd/. .
   git add .
   git commit -m "initial commit"
   git branch -m master main
   git push -u origin main
   ```

## Anthos Config Management

[Anthos Config Management](https://cloud.google.com/anthos/config-management) uses the `config` repository to deploy Kubernetes manifests to all Kubernetes clusters in the platform. This way, the `config` repo becomes the _source of truth/record_ for all application configuration. The files needed for the repository are located in the `/platform_admins/starter_repos/config` folder of the workshop repository. You copy the contents of this folder into the `config` repo in Gitlab (created in the workshop) to prepare the repo.

`config` repository. This repository is responsible for creating a _landing zone_ for each service to be deployed. The term _landing zone_ refers to a portion of the platform that is configured for a specific service. In Kubernetes, this could be a [namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/) as well as policies that may be required for a particular service. Each service gets deployed in its own namespace. Anthos Config Management [ACM](https://cloud.google.com/anthos/config-management) _config sync_ functionality is used to _pull_ Kubernetes config from the `config` repo and apply to the clusters. This ensures these namespaces are created in all clusters. `config` repo is also used to deploy the final _hydrated_ service configs to the clusters. Hydrated Kubernetes config refers to the final intended state for a service.

## config repository

1. Run the following commands to initialize the `config` repository.

   ```bash
   source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
   cd ${WORKDIR}
   # init git
   git config --global user.email "${GCLOUD_USER}"
   git config --global user.name "Cloud Shell"
   if [ ! -d ${HOME}/.ssh ]; then
     mkdir ${HOME}/.ssh
     chmod 700 ${HOME}/.ssh
   fi
   # pre-grab gitlab public key
   ssh-keyscan -t ecdsa-sha2-nistp256 -H gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog >> ~/.ssh/known_hosts
   git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:platform-admins/config.git
   cd ${WORKDIR}/config
   touch README.md
   git add .
   git commit -m "initial commit"
   git branch -m master main
   git push -u origin main
   git checkout -b prep
   cp -r ${WORKDIR}/anthos-multicloud-workshop/platform_admins/starter_repos/config/. .
   git add .
   git commit -m 'apply appropriate istio rev label'
   # issues with auto-merge -- https://gitlab.com/gitlab-org/gitlab/-/issues/260311
   git push -u origin prep -o merge_request.create -o merge_request.merge_when_pipeline_succeeds -o merge_request.target=main
   ```

   Example output:

   ```bash output
   remote:
   remote: View merge request for prep:
   remote:   https://gitlab.endpoints.qwiklabs-gcp-02-3b55f8d468d3.cloud.goog/platform-admins/config/-/merge_requests/1
   remote:
   To gitlab.endpoints.qwiklabs-gcp-02-3b55f8d468d3.cloud.goog:platform-admins/config.git
   * [new branch]      prep -> prep
   Branch 'prep' set up to track remote branch 'prep' from 'origin'.
   ```

2. Access gitlab with the provided link and select `Merge` to update the applicable namespace istio revision label.

   ```bash
   echo -e "https://gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog/platform-admins/config/-/merge_requests/1"
   ```

   <img alt="Image" src="img/gitlab-config-merge.png" width=50% height=50%>

3. Monitor the status of the ACM synchronization to the `main` branch of the `config` repo.

   ```bash
   echo "Check the status of the ACM synchronization at https://console.cloud.google.com/anthos/config_management"
   ```

4. Ensure all clusters show **Status** as `SYNCED` to the `main` branch of the `config` repo. You can also run the following command.

   ```bash
   gcloud beta container hub config-management status
   ```

   You may need to run this command multiple times until clusters are synced. The output should look as follows:

   ```bash output
   Name                    Status  Last_Synced_Token  Sync_Branch  Last_Synced_Time      Policy_Controller  Hierarchy_Controller
   eks-prod-us-west2ab-1   SYNCED  81cde9b            main         2021-08-02T21:43:43Z  INSTALLED          NA
   eks-prod-us-west2ab-2   SYNCED  81cde9b            main         2021-08-02T21:43:48Z  INSTALLED          NA
   eks-stage-us-east1ab-1  SYNCED  81cde9b            main         2021-08-02T21:43:47Z  INSTALLED          NA
   gke-dev-us-west1a-1     SYNCED  81cde9b            main         2021-08-02T21:43:45Z  INSTALLED          NA
   gke-dev-us-west1b-2     SYNCED  81cde9b            main         2021-08-02T21:43:42Z  INSTALLED          NA
   gke-prod-us-west2a-1    SYNCED  81cde9b            main         2021-08-02T21:43:52Z  INSTALLED          NA
   gke-prod-us-west2b-2    SYNCED  81cde9b            main         2021-08-02T21:43:44Z  INSTALLED          NA
   gke-stage-us-east4b-1   SYNCED  81cde9b            main         2021-08-02T21:43:41Z  INSTALLED          NA
   ```

   Syncing to the `config` repo ensures that the namespaces for the Online Boutique application are created on all clusters. You can now deploy the Online Boutique app.

## Redis

[Redis](https://redis.io/) is an open source in-memory key-value datastore. You can deploy Redis in a multicloud platform in a number of ways. One of these ways is to deploy redis as a [cluster](https://redis.io/topics/cluster-spec). A Redis cluster consists of one or more primary nodes and one or more corresponding replica nodes. In a multi-primary Redis cluster, the primary nodes divide the [hash slots](https://redis.io/topics/data-types-intro) amongst themselves. There are 0 - 16383 hash slots. So in a three node Redis cluster, each node takes ownership of approximately 5640 hash slots.

You can deploy Redis inside Kubernetes or outside Kubernetes. In the Anthos multicloud platform, you deploy a 3 node Redis cluster each with 1 replica node in Kubernetes clusters. The 3 primary nodes are deployed in a GKE cluster and their replicas are deployed in an EKS cluster as [StatefulSets](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/). You can learn more about deploying Redis on GKE clusters [here](https://rancher.com/blog/2019/deploying-redis-cluster).

![Image](img/traffic_flow.png)

## redis repository

1. Run the following commands to create a `redis` repository where all of the configuration files reside.

```bash
source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
cd ${WORKDIR}
# init git
git config --global user.email "${GCLOUD_USER}"
git config --global user.name "Cloud Shell"
if [ ! -d ${HOME}/.ssh ]; then
  mkdir ${HOME}/.ssh
  chmod 700 ${HOME}/.ssh
fi
# pre-grab gitlab public key
ssh-keyscan -t ecdsa-sha2-nistp256 -H gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog >> ~/.ssh/known_hosts
git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:databases/redis.git
cd ${WORKDIR}/redis
cp -r ${WORKDIR}/anthos-multicloud-workshop/platform_admins/starter_repos/redis/. .
git add .
git commit -m "initial commit"
git branch -m master main
git push -u origin main
```

Upon committing to the `redis` repo, a CI/CD pipeline is triggered which deploys the Redis cluster on `$GKE_PROD_1` (3 primary nodes) and `$EKS_PROD_1` (three replica nodes) clusters.

1. You can view the CI/CD pipeline by navigating to the output of the following link.

```bash
echo -e "https://gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog/databases/redis/-/pipelines"
```

1. Wait until the pipeline finishes successfully.

1. Run the following command to verify that Redis cluster is properly created.

```bash
kubectl --context=${GKE_PROD_1} -n db-redis exec -it gke-redis-0 -- redis-cli cluster nodes
```

Example output:

```bash output
1abf9848685eb9f6b159d4b5a0d7f827eb297066 240.0.0.11:6379@16379 slave 6d6a3ddc32f2c8ad069c299b24d2a5e3d63bbb6d 0 1604876627582 1 connected
13e3b23359f74e0473b8dab44809497d9a38fcd8 240.0.0.21:6379@16379 master - 0 1604876626000 2 connected 5461-10922
c8c04e23bdf02345fdd0495eec1d896d65238924 240.0.0.22:6379@16379 slave 1e5be7edf0e218626d95cf84e4471a60e3557145 0 1604876625542 7 connected
6d6a3ddc32f2c8ad069c299b24d2a5e3d63bbb6d 240.0.0.20:6379@16379 myself,master - 0 1604876625000 1 connected 0-5460
cdbe0345d0e399ecacdd13bd6703940ab419fffa 240.0.0.12:6379@16379 slave 13e3b23359f74e0473b8dab44809497d9a38fcd8 0 1604876626000 2 connected
1e5be7edf0e218626d95cf84e4471a60e3557145 240.0.0.10:6379@16379 master - 0 1604876626580 7 connected 10923-16383
```

You see 6 nodes. There are three primary nodes (labeled `master`) with IP addresses 240.0.0.20, 21 and 22 and three replica nodes (labeled `slave`) with IP addresses 240.0.0.10, 11 and 12. You can see the hash slot for each primary node. For example, in the output above the primary node 240.0.0.20 has slots `0-5460`, node with IP 240.0.0.21 has slots `5461-10922` and node with IP 240.0.0.22 has slots `10923-16383`.

## Multicloud Continuous Delivery

You intend to deploy an application called Online Boutique in your multicloud platform. [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo) is a sample e-commerce application that simulates an online store. The applications lets users browsers items, get details about items, place items in a shopping cart, checkout or purchase items that are in their shopping cart and get recommendations based on their purchase and browsing history. The application is composed of 10 services that each run as containers and connect to each other over REST and GRPC API. There is an 11th service called `loadenerator` which (as the name suggests) is used to simulate user load to the application. This is helpful when verifying functionality as well as looking at telemetry of the application.

You use Git to deploy the application. The source code for each service as well as the configuration needed to deploy and run the service are stored in Git repositories. In this workshop, you use Gitlab, however, you may use any Git solution for example Gitlab or Cloud Source Repositories. Once the source code and the services' configuration are stored in Git, CI workers (containers with the required tooling and access to the platform) are used to perform a series of steps to ensure that the application and its configuration is deployed in a consistent and repeatable manner. These series of steps can be automatically triggered by a user event. With Git, these events are when a user _commits_ something to the repository, when a user creates a _pull request_ or a _merge request_ and when that _pull/merge request_ is accepted. Different steps can be taken for each of these events depending upon the intent. This setup allows developers and application owners to continuously deploy applications without the need to coordinate with operations or by conducting any manual steps. These series of steps are themselves stored as code and are sometimes called CI/CD pipelines. CI (Continuous Integration) refers to the **building** of a deployable artifact. And CD (Continuous Delivery) refers to the **deployment** of the artifact along with its configuration. In this lab, you focus on CD. The artifacts for each service are containers that are already created and stored in a container repository.

Your goal is to deploy the containerized services along with their configuration to the multicloud platform.

At a high level you perform the following steps:

1. `online-boutique` repository. Online boutique is the application you aim to deploy in your multicloud platform. This repository contains the source code (in the `/src` folder) and the service configuration files (in the `/services` folder) required to deploy all services.
1. Run the Gitlab CI pipeline in the `online-boutique` repo which generates hydrated Kubernetes manifest files for each service and commits them to the `config` repo in the appropriate service namespace.
1. Verify all Deployments are _Ready_.
1. Ensure Online Boutique is functioning by browsing through the app and performing all user actions.

## online-boutique Repository

The `online-boutique` repository is the application repository. This repository contains the source code (in the `/src` folder) as well as configuration files (in the `/services` folder) for all services.

### Deployment Pipeline

The deployment pipeline is defined in the `.gitlab-ci.yml` file. It consists of four stages:

1. **Build** Stage. This stage creates the _hydrated_ Kubernetes manifest files for all twelve services. Hydrated Kubernetes configuration is a single file (called `${SVC}-hydrated.yaml`) that contains all resources required to run the service (deployment, service, ingress, service account etc.). This stage also creates a Cloud Ops [dashboard](https://cloud.google.com/monitoring/charts/dashboards) for the [golden signals](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/) for each service. The script to create the dashboard as well as the dashboard template are located in the `/services/${SVC}/prod/monitoring` folder.
1. **Commit** Stage. In this stage, the hydrated Kubernetes manifest files (created and outputted in the **Build** stage) are committed to the `config` repository. There are twelve services in the Online Boutique application. Each service gets deployed to its own namespace. The name of the namespace is the same as the service name prefixed by `ob-` for example the `frontend` service is deployed in the `ob-frontend` namespace. Each service namespace folder is under the `/namespaces/online-boutique` folder in the `config` repository. The hydrtaed Kubernetes manifests files for each service is copied to the respective service namespace folders. For example, the `frontend-hydrated.yaml` file is copied to the `/namespaces/online-boutique/frontend` folder.
1. **WorkloadIdentity** Stage. Each service runs with a [Kubernetes Service Account](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/). The service account name is the same as the service name. For example, the `frontend` service runs in the `ob-frontend` namespace with the `frontend` service account. You can think of the Kubernetes Service Account as the service identity. Each service writes metrics to [Cloud Ops](https://cloud.google.com/products/operations). You need to authenticate and authorize services to write metrics data to Cloud Ops. Services running in GKE (on GCP) can use [Workload Identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity). With Workload Identity, you can configure a Kubernetes service account to act as a Google service account. As part of the workshop build pipeline, a GCP service account called `cloud-ops` is created with proper IAM roles to write metrics data to Cloud Ops. All services running on GKE (on GCP) use Workload Identity with the `cloud-ops` service account. This allows GKE services to write metrics data to Cloud Ops.
1. **SecureIngress** Stage. You access Online Boutique via the `frontend` service. This stage securely exposes the `frontend` service so that you can access it over HTTPS. For HTTPS access to the frontend, you require a domain name and a certificate signed by a well-known CA (recognized by common browsers). You use [Cloud Endpoints](https://cloud.google.com/endpoints/docs/openapi/cloud-goog-dns-configure) to get a free DNS name for your application. With the Cloud Endpoints DNS name, you can use Google-managed SSL certificates service to get a free certificate for the frontend. By default, `frontend` Pods are deployed on all four clusters (two GKE and two EKS clusters). You use [GCLB](https://cloud.google.com/load-balancing) to send all incoming traffic via the DNS name to the two `istio-ingressgateway` proxy Pods running on the two GKE clusters. `istio-ingressgateway` proxies are aware of all four `frontend` Pods and load balance traffic to all `frontend` Pods. The default load balancing mode is `ROUND ROBIN`. This is defined in the `destinationrule--frontend.yaml` (located in the `/services/frontend/templates` folder). The default split between Pods running in GKE and EKS is 50/50. This is defined in the `virtualservice--subset-patch.yaml` and `virtualservice--ingress-subset-patch.yaml` (located in the `/services/frontend/prod/traffic` folder). _DestinationRule_ and _VirtualService_ are [CRDs](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) used by Anthos Service Mesh [ASM](https://cloud.google.com/anthos/service-mesh). You can learn more about them [here](https://istio.io/latest/docs/reference/config/networking/virtual-service/).

The `.gitlab-ci.yml` pipeline runs every time a commit is made to the `online-boutique` repository.

1. Initialize the `config`, `shared-cd` and `redis` repositories. See the [Prerequisites](#prerequisites) section above for details.

1. Run the following commands to initialize the `online-boutique` repository.

```bash
cd ${WORKDIR}
git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:online-boutique/online-boutique.git
cd $WORKDIR/online-boutique
cp -r ${WORKDIR}/anthos-multicloud-workshop/platform_admins/starter_repos/online_boutique/. .
git add .
git commit -m "initial commit"
git branch -m master main
git push -u origin main
```

## Deployment Pipeline

Every time you commit to the `online-boutique` repository, you can view the pipeline by accessing the following link. You can also navigate to the same link by clicking on the **CI/CD > Pipelines** link from the left hand nav bar.

```bash
echo -e "https://gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog/online-boutique/online-boutique/-/pipelines"
```

Wait until the pipeline successfully completes.

> If the pipeline fails, for instance on the `SecureIngress` job, just retry the pipeline job.

<img alt="Image" src="img/ob-multicluster-cd-pipeline-success.png" width=70% height=70%>

You can click on individual jobs to view details.

## Viewing Workloads

After the pipeline successfully completes, click on the following link to get the workloads (i.e. Deployments) for the Online Boutique application.

```bash
echo -e "https://console.cloud.google.com/kubernetes/workload?cloudshell=false&project=${GOOGLE_PROJECT}&pageState=(%22savedViews%22:(%22i%22:%22848de3cf75db4b5389286a0e967df1bf%22,%22c%22:%5B%5D,%22n%22:%5B%22ob-ad%22,%22ob-cart%22,%22ob-checkout%22,%22ob-currency%22,%22ob-email%22,%22ob-frontend%22,%22ob-loadgenerator%22,%22ob-payment%22,%22ob-productcatalog%22,%22ob-recommendation%22,%22ob-redis%22,%22ob-shipping%22%5D))"
```

Each service for the Online Boutique gets deployed in its own namespace. The link above creates a view in the **Kubernetes Engine > Workloads** page with all of the Online Boutique services' namespaces selected.

<img alt="Image" src="img/online-boutique-workloads.png" width=70% height=70%>

Ensure all workloads are _Ready_. Refresh the screen until all workloads are healthy.

## Secure Ingress

The **SecureIngress** stage creates an implementation of multi-cloud ingress for the Online Boutique application. This creates a DNS name for the `frontend` service (using Cloud Endpoints) and a Google-managed certificate.

1. Check that the managed certificate is proivisioned and `ACTIVE` by running the following command:

```bash
gcloud compute ssl-certificates list
```

The output looks like the following:

```bash output
NAME                         TYPE     CREATION_TIMESTAMP             EXPIRE_TIME  MANAGED_STATUS
obfrontend-managed-ssl-cert  MANAGED  2020-10-31T17:12:30.862-07:00               ACTIVE
    shop.endpoints.gcp_project_id.cloud.goog: ACTIVE
```

2. Once the certificate is `ACTIVE`, you can access the Online Boutique application by navigating to the following link:

```bash
echo -e "https://shop.endpoints.${GOOGLE_PROJECT}.cloud.goog"
```

You can now navigate through the application and perform actions like browing through items, getting item details, placing them in your shopping cart and checking out.

> If you get the error `ERR_SSL_VERSION_OR_CIPHER_MISMATCH` when accessing the frontend, wait a few moments and then try again.

The traffic flow is diagrammed below:

![Image](img/traffic_flow.png)

1. Client accesses Online Boutique via a DNS name. In this workshop, the DNS name is provided by Cloud Endpoints, however, you can use any DNS name of a domain you own.
1. A Google-managed SSL certificate is used for HTTPS access to Online Boutique.
1. GCLB sends the traffic to the two `istio-ingressgateway` proxy Pods running in the two GKE clusters. You can control how much load goes to each backend (i.e. `istio-ingressgateway`) using the [RATE](https://cloud.google.com/load-balancing/docs/https#load_distribution_algorithm) setting.
1. Each `istio-ingressgateway` is aware of all `frontend` Pods. By default, `frontend` Pods are deployed to all four clusters. `frontend` is divided into two subsets - one for GCP and one for AWS. Subsets group workloads based on labels. All workloads (Pods) running GKE contain the label `provider: gcp` while all workloads running in EKS contain the label `provider: aws`. Using label selectors, the two subsets (one for Pods running in GCP and the other for Pods running in AWS) are created as part of the **Build** pipeline. You can then use a `VirtualService` CRD to control how much traffic you want to send to each subset. by default, the traffic is equally split between the two subsets.
1. From `client` to `frontend`, all traffic is encrypted. TLS between client and `istio-ingressateway` and mTLS between `istio-ingressgateway` and the `frontend` Pods.

Congratulations. You have successfully deployed a microservice-based application to the Anthos multi-cloud platform using a simple Continuous Delivery (CD) pipeline.
