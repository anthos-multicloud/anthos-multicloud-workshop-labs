# AMCW: Monitoring and service level objectives

## Objectives

1. Deploy a microservices based application in all environments split across multiple clusters.
1. Configure Cloud Monitoring to receive metrics from all services running in all clusters in all environments.
1. Create a metrics dashboard for Online Boutique app in all environments for `service request counts` and `service latency`.
1. Gain an understanding of the health of your services using Service Level Objectives(SLOs) for some canonical services deployed in the environment.
1. Observe how the Anthos Service Mesh (ASM) dashboard surfaces SLO information.
1. Change the behavior of a service using ASM to only respond correctly 50% of the time and generate an alert.

## Setup

- In the GCP console, open [Cloud Shell](https://shell.cloud.google.com/). This lab is intended to be run from Cloud Shell.

- Wait for the `vars.sh` file to be available.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      gsutil ls gs://${DEVSHELL_PROJECT_ID}/vars.sh &> /dev/null
      exit_code=$?

      clear

      if [ ${exit_code} -eq 0 ]; then
          echo "${TIMESTAMP} vars.sh file is available"  
          break
      fi

      echo "${TIMESTAMP} vars.sh file is not ready yet..."  
      sleep 5
  done
  ```

- Create a `WORKDIR` for this tutorial. All files related to this tutorial end up in `WORKDIR`.

  ```bash
  mkdir -p $HOME/anthos-multicloud && cd $HOME/anthos-multicloud && export WORKDIR=$HOME/anthos-multicloud
  ```

- Clone the workshop repo.

  ```bash
  git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop.git ${WORKDIR}/anthos-multicloud-workshop
  ```

- Download the `var.sh` file,

  ```bash
  gsutil cp gs://${DEVSHELL_PROJECT_ID}/vars.sh ${WORKDIR}/
  ```

- Source the `vars.sh` file.

  ```bash
  source ${WORKDIR}/vars.sh
  ```

- Install the required tools.

  ```bash
  mkdir -p ~/.cloudshell && touch ~/.cloudshell/no-apt-get-warning
  ${WORKDIR}/anthos-multicloud-workshop/platform_admins/scripts/tools.sh
  ```

## User setup

- Check that all of the builds have completed successfully.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      ONGOING_BUILDS=`gcloud builds list --ongoing 2> /dev/null`
      ONGOING_BUILDS_COUNT=$(echo -n "${ONGOING_BUILDS}" | wc -l)

      clear

      if [ "${ONGOING_BUILDS_COUNT}" -eq "0" ]; then
          echo "${TIMESTAMP} All builds have completed, check the status of the builds at https://console.cloud.google.com/cloud-build/builds"
          break
      fi

      echo -en "${TIMESTAMP} ${ONGOING_BUILDS_COUNT} build(s) still running, build progress can also be monitored at https://console.cloud.google.com/cloud-build/builds\n${ONGOING_BUILDS}\n"
      sleep 5
  done
  ```

  <img alt="Image" src="img/cloudbuild_success.png" width=70% height=70%>

- Run the `user_setup.sh` script.

  ```bash
  source ${HOME}/.bashrc
  source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
  ```

- This `user_setup.sh` script performs the following steps:

  - Downloads EKS cluster _kubeconfig_ files. The location of these files is in the `${WORKDIR}/kubeconfig` folder.
  - Downloads SSH-Key pair. SSH Keys are used to interact with Gitlab repos. The location of these files is in the `${WORKDIR}/ssh-keys` folder.
  - Downloads the Gitlab hostname and root password txt file. The location of the file is in the `${WORKDIR}/gitlab` folder.
  - Creates a combined _kubeconfig_ file with all cluster contexts. Renames the clusters for easy context switching. The location of the merged _kubeconfig_ file is `${WORKDIR}/kubeconfig/workshop-config`. The script also sets this as your `KUBECONFIG` variable.
  - Get the EKS cluster's Kubernetes Service Account tokens to login to through the Cloud Console. Learn about logging in to Anthos registered clusters [here](https://cloud.google.com/anthos/multicluster-management/console/logging-in).
    > The script is idempotent and can be run multiple times.

  Example output from the end of the `user_setup.sh` script:

  ```bash output
  *** eks-prod-us-west2ab-1 Token ***

  [EKS Cluster Token]

  *** eks-prod-us-west2ab-2 Token ***

  [EKS Cluster Token]

  *** eks-stage-us-east1ab-1 Token ***

  [EKS Cluster Token]

  *** Gitlab Hostname and root password ***

  gitlab.endpoints.PROJECT_ID.cloud.goog
  [`root` PASSWORD]
  ```

### Variables

The `user_setup.sh` script adds additional variables for the workshop to the `vars.sh` file. The `vars.sh` file is automatically sourced when you log in to Cloud Shell. You can also manually source the file.

### Logging in to EKS clusters

There are three EKS clusters in the architecture. Two clusters in the `prod` environment and one in the `stage` environment. The tokens from the `user_setup.sh` script can be used to log in to the EKS clusters in Cloud Console.

- Navigate to the **Kubernetes Engine > Clusters** page in Cloud Console. You can see the three EKS clusters registered. They have not been logged in.

  <img alt="Image" src="img/eks_loggedout.png">

- Click **Login** next to each EKS cluster and select **Token**. Copy and paste the tokens (outputted from the `user_setup.sh` script) to the EKS clusters.
- Note: Simply select the token in the cloud shell (and nothing else, this automatically copies is), and paste it into the token box in the Cloud Console without intermittently storing it into a file. Any intermittent storage might add extra characters that will invalidate the token and login will fail.

  <img alt="Image" src="img/eks_login.png" width=40% height=40%>

- Navigate to the **Workloads** and **Services** pages and verify you can see metadata information from the EKS clusters. This confirms you have successfully logged in.

  <img alt="Image" src="img/eks_workloads.png" width=80% height=80%>

## Deploying Online Boutique application

1. Deploy the [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo) sample application in all environments by running the following scripts.

   **Development**

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob_dev.sh
   ```

   **Staging**

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob_stage.sh
   ```

   **Production**

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob_prod.sh
   ```

   Online Boutique is a microservices based application composed of 11 microservices.
   These script deploy the Online Boutique application in all environments with microservices split across all clusters within each environment.
   Services are deployed on all clusters. Deployments are only created in one cluster within an environment for every microservice.
   The script ensures all Deployments are _Ready_ before exiting.

   Example output of the end of the deployment script:

   ```bash output
   *** Access Online Boutique app in namespace ob-prod by navigating to the following address: ***

   http://35.34.33.32
   ```

   You can access Online Boutique application through the `frontend` Service. The `frontend` Service is exposed via the `istio-ingressgateway` Service using a TCP L4 load balancer.
   The public IP address of the `istio-ingressgateway` Service is outputted by each script.

1. Copy and paste the public IP address of the `istio-ingressgateway` Service for each environment into a browser tab.

1. You should see the homepage of the Online Boutique application.

   <img alt="Image" src="img/ob-frontend.png" width=70% height=70%>

1. Navigate around the application to ensure complete functionality. You should be able to browse items, place them in cart, add additional items to the cart and checkout.

1. Inspect the **Workloads** for each environment through Cloud Console. Navigate to the **Kubernetes Engine > Workloads** page in Cloud Console.

   <img alt="Image" src="img/gke-workloads-menu.png" width=30% height=30%>

1. Online Boutique application is installed in different namespaces for different environments. The namespaces are names `ob-<env>`. For example, in `prod` environment, Online Boutique is installed in the namespace `ob-prod`. From the **Namespace** dropdown, select the `ob-prod` namespace and click **OK**.

   <img alt="Image" src="img/gke-namespace-dropdown.png" width=20% height=20%>

1. You can now see Deployments for all clusters (GKE and EKS) in the `prod` environment. Click `Cluster` in the table to sort by cluster. You can see that every Deployment is running on one (of four) clusters.

   <img alt="Image" src="img/gke-workloads-ob-prod.png" width=70% height=70%>

For Online Boutique to function, all microservices must be _Ready_ and be able to communicate across to other Services. See [Online Boutique Architecture](https://github.com/GoogleCloudPlatform/microservices-demo#service-architecture) for details on service to service connectivity. Anthos Service Mesh ([ASM](https://cloud.google.com/anthos/service-mesh)) allows you to create multi-cloud service mesh between Anthos and Anthos attached clusters running anywhere. In this case, ASM creates a multi-cloud service mesh between GKE and EKS clusters.

Anthos Service Mesh is designed to be _ambient_, meaning it operates transparently at the platform layer without interfering with the Services. This allows you to simply move a workload (a Deployment) from one cluster to another even across clouds wihtout any additional configuration.

## Create a Monitoring chart

1. Navigate to the **Monitoring [Overview](https://console.cloud.google.com/monitoring)** page.

   <img alt="Image" src="img/cloud-mon-init.png" width=30% height=30%>

1. Using the left navigation panel, click on **[Metrics Explorer](https://console.cloud.google.com/monitoring/metrics-explorer)**.

   <img alt="Image" src="img/cloud-mon-metrics-link.png" width=20% height=20%>

1. For **Resource type** find and select `Kubernetes Container`.

1. For **Metrics** find and select `Server Request Count`.

1. For **Group By** find and select `destination_service_name` and `cluster_name`.

1. Ensure that **Aggregator** is set to `sum` and **Minimum alignment period** is set to `1 minute`.

1. You can now see a line chart of server request counts grouped by service name and cluster.

   <img alt="Image" src="img/cloud-mon-chart-svc.png" width=50% height=50%>

1. Scroll through the table view and verify that you are getting metrics from all environments and clusters.

## Creating a Monitoring dashboard

1. Click on the **Save Chart** button on the upper right.

1. On the **Save Chart** screen click on the **Dashboard** dropdown and select **New Dashboard**.

1. For **Dashboard Name** enter `Online Boutique App`.

   <img alt="Image" src="img/cloud-mon-dash-svc.png" width=40% height=40%>

1. Click **Save**.

1. Using the left navigation panel, click on **[Dashboards](https://console.cloud.google.com/monitoring/dashboards)**.

1. Under **Categories** click `Custom` and then click on the `Online Boutique App` dashboard. Refresh the page if the chart does not show up.

   <img alt="Image" src="img/cloud-mon-dash-page.png" width=70% height=70%>

## Adding additional charts to the dashboard

The easiest way to add a chart is to clone an existing chart to the same dashboard and then editing its attributes.

1. In the upper right click **VIEWING** and then click **Switch to Editing mode**.

1. Click on the existing chart and click **CLONE CHART**.

1. For **Chart Title** enter `Latency 99th Percentile`.

1. For **Dashboard** ensure `Online Boutique App` is selected.

1. Click **CLONE**.

   <img alt="Image" src="img/cloud-mon-chart-clone-99.png" width=30% height=30%>

1. Click on the new chart to bring up the chart configuration on the left side of the screen.

1. For **Metric** clear `Server Request Count` and then find and select `Server Response Latencies`.

1. Under **Do you want it preprocessed?** change the **Preprocessing step** to `Percentile`

1. In the **Percentile** dropdown select `99th percentile`.

   <img alt="Image" src="img/cloud-mon-lat-99.png" width=30% height=30%>

1. The changes are autosaved.

1. Use **CLONE CHART** on the `Latency 99th Percentile` chart to create a `95th percentile` and `50th percentile` chart using the same process.

1. You should now have four charts in your dashboard.
   - Server Request counts
   - Latency 99th Percentile
   - Latency 95th Percentile
   - Latency 50th Percentile

1. In the upper right click **EDITING** and then click **Switch to Viewing mode**. Refresh the page if the charts do not show up.

   <img alt="Image" src="img/cloud-mon-dash-app.png" width=80% height=80%>

## Using filters

The current dashboard has four charts. Each charts is grouped by all services, across all clusters, across all environments. You can use **filters** to narrow down what you want to view.

In this example, lets assume you want to only see traffic flowing to the `frontend` service in the `prod` environment and all associated workloads.

1. In the **Filter** bar near the top, select `namespace_name` and click on `ob-prod`.

   <img alt="Image" src="img/cloud-mon-filter-ns.png" width=30% height=30%>

1. Add an additional filter for `destination_service_name: frontend`.

1. Click on the **Expand chart legend** button near the upper right corner of each charts to display the chart's legend.

## Scripted Dashboard for Production

A scripted dashboard for production is also available.

1. Create the scripted dashboard from [Cloud Shell](https://shell.cloud.google.com/)..

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ops/services-dashboard.sh \
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ops/services-dashboard-prod.json_tmpl
   ```

   Example output

   ```bash output
   ... [json] ...
   https://console.cloud.google.com/monitoring/dashboards/custom/servicesdash?cloudshell=false&project=qwiklabs-gcp-01-01f4f219d79d
   ```

1. Click on the link from the script output to open the dashboard directly.

1. The `Services Dashboard - Production` dashboard will be opened in a new tab.

## Create an SLO using the UI

1. Navigate from the hamburger menu **Anthos > Service Mesh**

1. Select `ob-dev` in the Namespace drop-down.

1. Notice some metrics already available with 3 of the 4 [golden signals](https://sre.google/sre-book/monitoring-distributed-systems/#xref_monitoring_golden-signals), Requests/sec, Error rate, and Latency.

    <img alt="Image" src="img/asm-slo-no-slo.png" width=20% height=20%>

1. Select `adservice` from the list of services

1. Select `Create An SLO`

    <img alt="Image" src="img/asm-slo-create-slo.png" width=30% height=30%>

1. We are going to set our Service Level Indicator, by determining the type of metric we want to set a performance goal on.  Select `Latency`, which is how long it takes to return a response to a request.

1. Next we need to set how we are going to set how the compliance is going to be measured, in this case `Request-based` and click `Continue`

    <img alt="Image" src="img/asm-slo-latency-request.png" width=30% height=30%>

1. SLI details are now required and we need to set our threshold.  ASM collects the raw data over a period of time and calculated as percentiles. Observe what the 50th, 95th and 99th percentiles are.  Enter a `3`ms for the latency threshold.

    > NOTE: Set a reasonable threshold, for instance the latency shouldn't be too low that it would be difficult to maintain with minimal impact to your end users / service.  Additional details on [setting SLI/SLOs](https://cloud.google.com/service-mesh/docs/observability/design-slo). 

    <img alt="Image" src="img/asm-slo-latency-threshold.png" width=30% height=30%>

1. [Compliance period](https://cloud.google.com/service-mesh/docs/observability/design-slo#compliance_periods) - In the lab we won't have enough time to generate enough data for a proper compliance period, but we will still need to set one.
    - Period type: Calendar
    - Period length: Calendar day

1. [Performance goal](https://cloud.google.com/stackdriver/docs/solutions/slo-monitoring?hl=he#defn-slo) - Setting a goal also creates an error budget, which is the total amount of time that a service can be non-compliant before it violates the SLO.  Set a goal of `80`% and click `Continue`

    > NOTE: A useful SLO is under 100%, because the SLO determines your error budget. SLOs are typically described as a “number of nines”: 99% (2 nines), 99.9% (3 nines), and so forth. The highest value you can set is 99.9%, but you can use any lower value that is appropriate for your service.

    <img alt="Image" src="img/asm-slo-compliance-goal.png" width=30% height=30%>

1. SLO details are presented, a generated JSON preview can also be used via API to create/update an SLO.  Click `Create SLO`.

    <img alt="Image" src="img/asm-slo-create-summary.png" width=30% height=30%>

1. Observe your SLO.

    <img alt="Image" src="img/asm-slo-created.png" width=30% height=30%>

## Use the APIs to create SLOs applicable services

1. You can manually use the UI to create individual SLOs.  However, we can use following script which uses the API to generate SLOs & alerts for the remaining applicable services.

    ```bash
    ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ops/asm-slo.sh \
      ${GOOGLE_PROJECT} ob-dev
    ```

    Example output:

    ```bash output
    ...
    POST LATENCY recommendationservice
    SLO_NAME is projects/745012302812/services/canonical-ist:proj-745012302812-ob-dev-recommendationservice/serviceLevelObjectives/K2n107PzTGmLvADUOh_T_w
    Created alert policy [projects/qwiklabs-gcp-01-ca67d11c5de3/alertPolicies/3685140218579828378].
    POST LATENCY shippingservice
    SLO_NAME is projects/745012302812/services/canonical-ist:proj-745012302812-ob-dev-shippingservice/serviceLevelObjectives/Ask7UzBTQba3qdzakemjcA
    Created alert policy [projects/qwiklabs-gcp-01-ca67d11c5de3/alertPolicies/12577200214731172412].
    ```

    > NOTE: In addition to creating a set of latency expectations for our services, we also created one avaialbility SLO for the `checkoutservice` of 99.9% for demonstration purposes.

1. Navigate from the hamburger menu **Anthos > Service Mesh**

1. Select `ob-dev` in the Namespace drop-down.

1. Observe the status of the respective services in the `ob-dev` namespace.  

    <img alt="Image" src="img/asm-slo-overview-slo.png" width=30% height=30%>

1. Services can have the following statuses

    <img alt="Image" src="img/asm-slo-statuses.png" width=30% height=30%>

## Trigger an alert

1. We need to create a problem, chaos monkey style, we will cause services (`frontend` and `checkoutservice`) to fail when they call `cartservice` 50% of the time.  In this lab, this will cause an alert to trigger on `checkoutservice` because it's having trouble reaching `cartservice` 50% of the time and we expect it to be avaialble 99.9% of the time.

1. Using Istio's `VirtualService` resource, we add a fault to the `cartservice`.

    ```bash
    kubectl --context ${GKE_DEV_1} \
      -n ob-dev \
      apply -f ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ops/virtualservice-cartservice-50fault.yaml
    ```

1. You will start to see error rates increase for `frontend` and `checkoutservice`.  For demonstration purposes we created a 99.9% availability expectation of the `checkoutservice`, via API, which is dependent on the `cartservice`.  This will trigger an alert.

    <img alt="Image" src="img/asm-slo-overview-alert.png" width=30% height=30%>

    > NOTE: It will take a few minutes (~3-5), for the checkout service to trigger the `checkoutservice` availability alert.

1. Test out the application in the browser, you will notice failure of the application about half the time.

    ```bash
    echo -n "http://" && \
    kubectl --context ${GKE_DEV_1} get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].ip'
    ```

1. It may take some time for the alert to show up for the `checkoutservice`.  It can also be accessed through the Cloud Monitoring component.

    ```bash
    echo -e https://console.cloud.google.com/monitoring/alerting?project=${GOOGLE_PROJECT}
    ```

    <img alt="Image" src="img/asm-slo-cloud-monitoring-incidents.png" width=30% height=30%>    

1. The service overview provides a general view.

    <img alt="Image" src="img/asm-slo-service-health-overview.png" width=30% height=30%>

1. The `checkoutservice` details

    <img alt="Image" src="img/asm-slo-alert-timeline.png" width=30% height=30%>