# AMCW: Configuration and policy with Anthos Config Management

## Objectives

1. Centrally manage configurations and policies for all your clusters with Anthos Config Management.

## Setup

- In the GCP console, open [Cloud Shell](https://shell.cloud.google.com/). This lab is intended to be run from Cloud Shell.

- Wait for the `vars.sh` file to be available.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      gsutil ls gs://${DEVSHELL_PROJECT_ID}/vars.sh &> /dev/null
      exit_code=$?
      
      clear

      if [ ${exit_code} -eq 0 ]; then
          echo "${TIMESTAMP} vars.sh file is available"  
          break
      fi
      
      echo "${TIMESTAMP} vars.sh file is not ready yet..."  
      sleep 5
  done
  ```

- Create a `WORKDIR` for this tutorial. All files related to this tutorial end up in `WORKDIR`.

  ```bash
  mkdir -p $HOME/anthos-multicloud && cd $HOME/anthos-multicloud && export WORKDIR=$HOME/anthos-multicloud
  ```

- Clone the workshop repo.

  ```bash
  git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop.git ${WORKDIR}/anthos-multicloud-workshop
  ```

- Download the `var.sh` file,

  ```bash
  gsutil cp gs://${DEVSHELL_PROJECT_ID}/vars.sh ${WORKDIR}/
  ```

- Source the `vars.sh` file.

  ```bash
  source ${WORKDIR}/vars.sh
  ```

- Install the required tools.

  ```bash
  mkdir -p ~/.cloudshell && touch ~/.cloudshell/no-apt-get-warning
  ${WORKDIR}/anthos-multicloud-workshop/platform_admins/scripts/tools.sh
  ```

## User setup

- Check that all of the builds have completed successfully.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      ONGOING_BUILDS=`gcloud builds list --ongoing 2> /dev/null`
      ONGOING_BUILDS_COUNT=$(echo -n "${ONGOING_BUILDS}" | wc -l)

      clear

      if [ "${ONGOING_BUILDS_COUNT}" -eq "0" ]; then
          echo "${TIMESTAMP} All builds have completed, check the status of the builds at https://console.cloud.google.com/cloud-build/builds"
          break
      fi

      echo -en "${TIMESTAMP} ${ONGOING_BUILDS_COUNT} build(s) still running, build progress can also be monitored at https://console.cloud.google.com/cloud-build/builds\n${ONGOING_BUILDS}\n"
      sleep 5
  done
  ```

  <img alt="Image" src="img/cloudbuild_success.png" width=70% height=70%>

- Run the `user_setup.sh` script.

  ```bash
  source ${HOME}/.bashrc
  source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
  ```

- This `user_setup.sh` script performs the following steps:

  - Downloads EKS cluster _kubeconfig_ files. The location of these files is in the `${WORKDIR}/kubeconfig` folder.
  - Downloads SSH-Key pair. SSH Keys are used to interact with Gitlab repos. The location of these files is in the `${WORKDIR}/ssh-keys` folder.
  - Downloads the Gitlab hostname and root password txt file. The location of the file is in the `${WORKDIR}/gitlab` folder.
  - Creates a combined _kubeconfig_ file with all cluster contexts. Renames the clusters for easy context switching. The location of the merged _kubeconfig_ file is `${WORKDIR}/kubeconfig/workshop-config`. The script also sets this as your `KUBECONFIG` variable.
  - Get the EKS cluster's Kubernetes Service Account tokens to login to through the Cloud Console. Learn about logging in to Anthos registered clusters [here](https://cloud.google.com/anthos/multicluster-management/console/logging-in).
    > The script is idempotent and can be run multiple times.

  Example output from the end of the `user_setup.sh` script:

  ```bash output
  *** eks-prod-us-west2ab-1 Token ***

  [EKS Cluster Token]

  *** eks-prod-us-west2ab-2 Token ***

  [EKS Cluster Token]

  *** eks-stage-us-east1ab-1 Token ***

  [EKS Cluster Token]

  *** Gitlab Hostname and root password ***

  gitlab.endpoints.PROJECT_ID.cloud.goog
  [`root` PASSWORD]
  ```

### Variables

The `user_setup.sh` script adds additional variables for the workshop to the `vars.sh` file. The `vars.sh` file is automatically sourced when you log in to Cloud Shell. You can also manually source the file.

### Logging in to EKS clusters

There are three EKS clusters in the architecture. Two clusters in the `prod` environment and one in the `stage` environment. The tokens from the `user_setup.sh` script can be used to log in to the EKS clusters in Cloud Console.

- Navigate to the **Kubernetes Engine > Clusters** page in Cloud Console. You can see the three EKS clusters registered. They have not been logged in.

  <img alt="Image" src="img/eks_loggedout.png">

- Click **Login** next to each EKS cluster and select **Token**. Copy and paste the tokens (outputted from the `user_setup.sh` script) to the EKS clusters.
- Note: Simply select the token in the cloud shell (and nothing else, this automatically copies is), and paste it into the token box in the Cloud Console without intermittently storing it into a file. Any intermittent storage might add extra characters that will invalidate the token and login will fail.

  <img alt="Image" src="img/eks_login.png" width=40% height=40%>

- Navigate to the **Workloads** and **Services** pages and verify you can see metadata information from the EKS clusters. This confirms you have successfully logged in.

  <img alt="Image" src="img/eks_workloads.png" width=80% height=80%>

## Initialize the config repository

1. Configure git.

   ```bash
   git config --global user.email "${GCLOUD_USER}"
   git config --global user.name "Qwiklabs Student"
   ```

1. Configure SSH.

   ```bash
   if [ ! -d ${HOME}/.ssh ]; then
       mkdir ${HOME}/.ssh
       chmod 700 ${HOME}/.ssh
   fi
   ssh-keyscan -t ecdsa-sha2-nistp256 -H gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog >> ${HOME}/.ssh/known_hosts
   ```

1. Clone the `config` repository.

   ```bash
   cd ${WORKDIR}
   git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:platform-admins/config.git
   ```

1. Initialize the `config` repository.

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/scripts/acm_config_repo.sh
   cd ${WORKDIR}/config
   cp -r ${WORKDIR}/anthos-multicloud-workshop/platform_admins/starter_repos/config_init/. ${WORKDIR}/config/

1. Push the changes.

   ```bash
   cd ${WORKDIR}/config
   git add .
   git commit -m "Initialize the repository"
   git branch -m master main
   git push -u origin main
   ```

1. Monitor the status of the ACM synchronization to the `main` branch of the `config` repo.

   ```bash
   echo && \
   echo "Check the status of the ACM synchronization at https://console.cloud.google.com/anthos/config_management" && \
   echo
   ```

1. Ensure all clusters show **Status** as `SYNCED` to the `main` branch of the `config` repo. You can also run the following command.

   ```bash
   gcloud beta container hub config-management status
   ```

   You may need to run this command multiple times until clusters are synced. The output should look as follows:

   ```bash output
   Name                    Status  Last_Synced_Token  Sync_Branch  Last_Synced_Time      Policy_Controller  Hierarchy_Controller
   eks-prod-us-west2ab-1   SYNCED  81cde9b            main         2021-08-02T21:43:43Z  INSTALLED          NA
   eks-prod-us-west2ab-2   SYNCED  81cde9b            main         2021-08-02T21:43:48Z  INSTALLED          NA
   eks-stage-us-east1ab-1  SYNCED  81cde9b            main         2021-08-02T21:43:47Z  INSTALLED          NA
   gke-dev-us-west1a-1     SYNCED  81cde9b            main         2021-08-02T21:43:45Z  INSTALLED          NA
   gke-dev-us-west1b-2     SYNCED  81cde9b            main         2021-08-02T21:43:42Z  INSTALLED          NA
   gke-prod-us-west2a-1    SYNCED  81cde9b            main         2021-08-02T21:43:52Z  INSTALLED          NA
   gke-prod-us-west2b-2    SYNCED  81cde9b            main         2021-08-02T21:43:44Z  INSTALLED          NA
   gke-stage-us-east4b-1   SYNCED  81cde9b            main         2021-08-02T21:43:41Z  INSTALLED          NA
   ```

   Syncing to the `config` repo ensures that the repo has been initialized.

## Create a namespace in all clusters

In this step you'll create a namespace, commit it to the repository and watch it apply to all clusters.

1. Create the `team1` namespace in all clusters.

   ```bash
   cd ${WORKDIR}/config
   mkdir -p namespaces/team1
   cat << EOF > namespaces/team1/namespace.yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: team1
   EOF
   ```

1. Commit the changes to `config` repository.

   ```bash
   git add namespaces/team1/namespace.yaml
   git commit -m "Created the team1 namespace in all clusters"
   git push
   ```

1. Monitor the status of the ACM synchronization to the `main` branch of the `config` repo.

   ```bash
   echo && \
   echo "Check the status of the ACM synchronization at https://console.cloud.google.com/anthos/config_management" && \
   echo
   ```

1. Ensure all clusters show **Status** as `SYNCED` to the `main` branch of the `config` repo. You can also run the following command.

   ```bash
   gcloud beta container hub config-management status
   ```

   You may need to run this command multiple times until clusters are synced. The output should look as follows:

   ```bash output
   Name                    Status  Last_Synced_Token  Sync_Branch  Last_Synced_Time      Policy_Controller  Hierarchy_Controller
   eks-prod-us-west2ab-1   SYNCED  81cde9b            main         2021-08-02T21:43:43Z  INSTALLED          NA
   eks-prod-us-west2ab-2   SYNCED  81cde9b            main         2021-08-02T21:43:48Z  INSTALLED          NA
   eks-stage-us-east1ab-1  SYNCED  81cde9b            main         2021-08-02T21:43:47Z  INSTALLED          NA
   gke-dev-us-west1a-1     SYNCED  81cde9b            main         2021-08-02T21:43:45Z  INSTALLED          NA
   gke-dev-us-west1b-2     SYNCED  81cde9b            main         2021-08-02T21:43:42Z  INSTALLED          NA
   gke-prod-us-west2a-1    SYNCED  81cde9b            main         2021-08-02T21:43:52Z  INSTALLED          NA
   gke-prod-us-west2b-2    SYNCED  81cde9b            main         2021-08-02T21:43:44Z  INSTALLED          NA
   gke-stage-us-east4b-1   SYNCED  81cde9b            main         2021-08-02T21:43:41Z  INSTALLED          NA
   ```

   Syncing to the `config` repo ensures that the `team1` namespace is created on all clusters.

## Use ClusterSelectors to deploy a namespace to specific clusters only

1. Create the `team1-dev` namespace in the dev clusters.

   ```bash
   cd ${WORKDIR}/config
   mkdir -p namespaces/team1-dev
   cat << EOF > namespaces/team1-dev/namespace.yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: team1-dev
     annotations:
       configmanagement.gke.io/cluster-selector: dev
   EOF
   ```

1. Commit the changes to `config` repository.

   ```bash
   git add namespaces/team1-dev/namespace.yaml
   git commit -m "Created the team1-dev namespace in the dev clusters"
   git push
   ```

1. Monitor the status of the ACM synchronization to the `main` branch of the `config` repo.

   ```bash
   echo && \
   echo "Check the status of the ACM synchronization at https://console.cloud.google.com/anthos/config_management" && \
   echo
   ```

1. Ensure all clusters show **Status** as `SYNCED` to the `main` branch of the `config` repo. You can also run the following command.

   ```bash
   gcloud beta container hub config-management status
   ```

   You may need to run this command multiple times until clusters are synced. The output should look as follows:

   ```bash output
   Name                    Status  Last_Synced_Token  Sync_Branch  Last_Synced_Time      Policy_Controller  Hierarchy_Controller
   eks-prod-us-west2ab-1   SYNCED  81cde9b            main         2021-08-02T21:43:43Z  INSTALLED          NA
   eks-prod-us-west2ab-2   SYNCED  81cde9b            main         2021-08-02T21:43:48Z  INSTALLED          NA
   eks-stage-us-east1ab-1  SYNCED  81cde9b            main         2021-08-02T21:43:47Z  INSTALLED          NA
   gke-dev-us-west1a-1     SYNCED  81cde9b            main         2021-08-02T21:43:45Z  INSTALLED          NA
   gke-dev-us-west1b-2     SYNCED  81cde9b            main         2021-08-02T21:43:42Z  INSTALLED          NA
   gke-prod-us-west2a-1    SYNCED  81cde9b            main         2021-08-02T21:43:52Z  INSTALLED          NA
   gke-prod-us-west2b-2    SYNCED  81cde9b            main         2021-08-02T21:43:44Z  INSTALLED          NA
   gke-stage-us-east4b-1   SYNCED  81cde9b            main         2021-08-02T21:43:41Z  INSTALLED          NA
    ```

   Syncing to the `config` repo ensures that the `team1-dev` namespace is created on both dev clusters.

1. Check namespaces on $GKE_DEV_1 cluster and find `team1-dev` namespace

   ```bash
   kubectl --context=$GKE_DEV_1 get ns
   ```

   Note in the output you see `team1-dev` namespace.

1. Check namespaces on $GKE_PROD_1 cluster and note that you will not find the `team1-dev` namespace.

   ```bash
   kubectl --context=$GKE_PROD_1 get ns
   ```

   Note in the output you do **not** see the `team1-dev` namespace.

## Drift Management

1. List namespaces on the $GKE_DEV_1 cluster

   ```bash
   kubectl --context=$GKE_DEV_1 get ns
   ```

   Note that the `team1-dev` namespace exists on the cluster.

1. Delete the `team1-dev` namespace

   ```bash
   kubectl --context=$GKE_DEV_1 delete ns team1-dev
   ```

1. List the namespaces one more time.

   ```bash
   kubectl --context=$GKE_DEV_1 get ns
   ```

   Note that the `team1-dev` namespace is still there on the cluster but notice that its age is more recent than before.
