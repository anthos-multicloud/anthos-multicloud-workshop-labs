# Anthos Multicloud Workshop Labs

These labs are adapted from the original [Anthos Multicloud Workshop](https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop) GitLab format for use in Qwiklabs. The lab numbers are not necessarily the order in which the labs are run, a workshop can be any compilation of the labs.

* [lab_00: Anthos Multicloud Workshop(AMCW) Overview](lab_00)
* [lab_01: AMCW: Building the foundation](lab_01)
* [lab_02: AMCW: Multicloud Online Boutique](lab_02)
* [lab_03: AMCW: Adding new cluster](lab_03)
* [lab_04: AMCW: Online Boutique CI/CD Pipeline](lab_04)
* [lab_05: AMCW: Bank of Anthos CI/CD Pipeline](lab_05)
* [lab_06: AMCW: Monitoring and service level objectives](lab_06)
* [lab_07: AMCW: Configuration and policy with Anthos Config Management](lab_07)

An example of a full workshop would be:

* [Anthos Multicloud Workshop(AMCW) Overview](lab_00)
* [AMCW: Building the foundation](lab_01)
* [AMCW: Multicloud Online Boutique](lab_02)
* [AMCW: Monitoring and service level objectives](lab_06)
* [AMCW: Configuration and policy with Anthos Config Management](lab_07)
* [AMCW: Adding new cluster](lab_03)
* [(Optional)AMCW: Online Boutique CI/CD Pipeline](lab_04)
* [(Optional)AMCW: Bank of Anthos CI/CD Pipeline](lab_05)

## Automation

There is also automation to:

* [Convert the labs from GitLab markdown to Qwiklabs markdown](qwiklabs/converter)
* [Enable hot labs, in Qwiklabs, using prewarming logic](qwiklabs/prewarm)
