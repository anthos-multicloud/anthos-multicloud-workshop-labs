# AMCW: Building the foundation

## Objectives

- Learn, build and manage an Anthos platform in a multicloud environment.

## Architecture

![Image](img/architecture.png)

## Building the Anthos platform

To set up the Anthos platform foundation, you accomplish the following:

- Setting up an Anthos multi-environment multicloud environment on GCP and AWS using GKE and EKS (Anthos attached clusters, registered via Hub).
- Setting up VPCs in each environment in GCP and AWS.
- Setting up GKE and EKS clusters in GCP and AWS.
- Deploying Anthos Config Management (ACM) on all clusters.
- Setting up a local Gitlab for source code management (SCM) with repos.
- Creating multicloud service meshes per environment using Anthos Service Mesh (ASM)

## Setup

- In the GCP console, open [Cloud Shell](https://shell.cloud.google.com/). This lab is intended to be run from Cloud Shell.

- Wait for the `vars.sh` file to be available.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      gsutil ls gs://${DEVSHELL_PROJECT_ID}/vars.sh &> /dev/null
      exit_code=$?
      
      clear

      if [ ${exit_code} -eq 0 ]; then
          echo "${TIMESTAMP} vars.sh file is available"  
          break
      fi
      
      echo "${TIMESTAMP} vars.sh file is not ready yet..."  
      sleep 5
  done
  ```

- Create a `WORKDIR` for this tutorial. All files related to this tutorial end up in `WORKDIR`.

  ```bash
  mkdir -p $HOME/anthos-multicloud && cd $HOME/anthos-multicloud && export WORKDIR=$HOME/anthos-multicloud
  ```

- Clone the workshop repo.

  ```bash
  git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop.git ${WORKDIR}/anthos-multicloud-workshop
  ```

- Download the `var.sh` file,

  ```bash
  gsutil cp gs://${DEVSHELL_PROJECT_ID}/vars.sh ${WORKDIR}/
  ```

- Source the `vars.sh` file.

  ```bash
  source ${WORKDIR}/vars.sh
  ```

## Deploying the environment

- Run the `build.sh` script from the root folder to set up the environment in GCP and AWS. The `build.sh` script runs the [`bootstrap.sh`](https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop/-/tree/main/platform_admins/scripts/bootstrap.sh) script, for more information about the script see the [bootstrap.sh documentations](https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop/-/tree/main/platform_admins/docs/scripts.md#bootstrapsh)

  ```bash
  cd ${WORKDIR}/anthos-multicloud-workshop
  ./build.sh
  ```

> Note that the infrastructure build process can take approximately 30 - 35 minutes to complete.

- After the `build.sh` script finishes, navigate to the **Cloud Build** details page in Cloud Console from the left hand navbar.
- Initially, you see the `main` build running. Click on the build ID to inspect the stages of the pipeline. The `main` build pipeline trigger additional builds.
- The following diagram illustrates the builds and the approximate times each stage takes to complete. Note that all the `env` and the `gitlab` stages run concurrently as shown.

  ![Image](img/build.png)

- You can trigger this pipeline by running the `build.sh` script which commits the changes to the `infrastructure` CSR repo's _main_ branch.
- Alternatively, you can directly commit changes to the `infrastructure` repo which is cloned in the `${WORKDIR}/infra-repo` folder in Cloud Shell.

> Running the `build.sh` script overrides any changes you make locally through the `infra-repo` folder.

## Infrastructure Pipeline

The following illustration provides a detailed view of the pipelines and the resources that are created, for more information see the [infrastructure pipeline documentation](https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop/-/tree/main/platform_admins/docs/pipelines.md#infrastructure-pipeline).

![Image](img/infrastructure_pipeline.png)

## User setup

[📽️ Video Walk Through: User setup](https://app.threadit.area120.com/thread/xxkgaitp7spclcmt7vqj?utm_medium=referral-link)

- Check that all of the builds have completed successfully.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      ONGOING_BUILDS=`gcloud builds list --ongoing 2> /dev/null`
      ONGOING_BUILDS_COUNT=$(echo -n "${ONGOING_BUILDS}" | wc -l)

      clear

      if [ "${ONGOING_BUILDS_COUNT}" -eq "0" ]; then
          echo "${TIMESTAMP} All builds have completed, check the status of the builds at https://console.cloud.google.com/cloud-build/builds"
          break
      fi

      echo -en "${TIMESTAMP} ${ONGOING_BUILDS_COUNT} build(s) still running, build progress can also be monitored at https://console.cloud.google.com/cloud-build/builds\n${ONGOING_BUILDS}\n"
      sleep 5
  done
  ```

  <img alt="Image" src="img/cloudbuild_success.png" width=70% height=70%>

- Run the `user_setup.sh` script.

  ```bash
  source ${HOME}/.bashrc
  source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
  ```

- This `user_setup.sh` script performs the following steps:

  - Downloads EKS cluster _kubeconfig_ files. The location of these files is in the `${WORKDIR}/kubeconfig` folder.
  - Downloads SSH-Key pair. SSH Keys are used to interact with Gitlab repos. The location of these files is in the `${WORKDIR}/ssh-keys` folder.
  - Downloads the Gitlab hostname and root password txt file. The location of the file is in the `${WORKDIR}/gitlab` folder.
  - Creates a combined _kubeconfig_ file with all cluster contexts. Renames the clusters for easy context switching. The location of the merged _kubeconfig_ file is `${WORKDIR}/kubeconfig/workshop-config`. The script also sets this as your `KUBECONFIG` variable.
  - Get the EKS cluster's Kubernetes Service Account tokens to login to through the Cloud Console. Learn about logging in to Anthos registered clusters [here](https://cloud.google.com/anthos/multicluster-management/console/logging-in).
    > The script is idempotent and can be run multiple times.

  Example output from the end of the `user_setup.sh` script:

  ```bash output
  *** eks-prod-us-west2ab-1 Token ***

  [EKS Cluster Token]

  *** eks-prod-us-west2ab-2 Token ***

  [EKS Cluster Token]

  *** eks-stage-us-east1ab-1 Token ***

  [EKS Cluster Token]

  *** Gitlab Hostname and root password ***

  gitlab.endpoints.PROJECT_ID.cloud.goog
  [`root` PASSWORD]
  ```

### Variables

The initial `build.sh` script creates a `vars.sh` file in the `${WORKDIR}` folder. The `user_setup.sh` script adds additional variables for the workshop to the `vars.sh` file. The `vars.sh` file is automatically sourced when you log in to Cloud Shell. You can also manually source the file.

### Logging in to EKS clusters

There are three EKS clusters in the architecture. Two clusters in the `prod` environment and one in the `stage` environment. The tokens from the `user_setup.sh` script can be used to log in to the EKS clusters in Cloud Console.

- Navigate to the **Kubernetes Engine > Clusters** page in Cloud Console. You can see the three EKS clusters registered. They have not been logged in.

  <img alt="Image" src="img/eks_loggedout.png">

- Click **Login** next to each EKS cluster and select **Token**. Copy and paste the tokens (outputted from the `user_setup.sh` script) to the EKS clusters.
- Note: Simply select the token in the cloud shell (and nothing else, this automatically copies is), and paste it into the token box in the Cloud Console without intermittently storing it into a file. Any intermittent storage might add extra characters that will invalidate the token and login will fail.

  <img alt="Image" src="img/eks_login.png" width=40% height=40%>

- Navigate to the **Workloads** and **Services** pages and verify you can see metadata information from the EKS clusters. This confirms you have successfully logged in.

  <img alt="Image" src="img/eks_workloads.png" width=80% height=80%>

### Logging in to Gitlab

Log in to Gitlab.

- Navigate to the output link of the following command.

  ```bash
  echo -e "https://gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog"
  ```

- Log in with the username `root` and the gitlab password that was near the end of the `user_setup.sh` script's output. The password can also be found in the `${WORKDIR}/gitlab/gitlab_cred.txt` file.
- You see multiple projects (or repos).

  <img alt="Image" src="img/gitlab_repos.png" width=50% height=50%>

## \[Optional\] Testing

The platform includes a test pipeline that can also be used to prepare the environment.

### Start the pipeline

1. A pipeline has already been defined to deploy the applications and their dependencies. Run the following commands to initialize the `test-harness` repository.

    ```bash
    source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
    cd ${WORKDIR}
    # init git
    git config --global user.email "${GCLOUD_USER}"
    git config --global user.name "Cloud Shell"
    if [ ! -d ${HOME}/.ssh ]; then
      mkdir ${HOME}/.ssh
      chmod 700 ${HOME}/.ssh
    fi
    # pre-grab gitlab public key
    ssh-keyscan -t ecdsa-sha2-nistp256 -H gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog >> ~/.ssh/known_hosts
    git clone git@gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog:test-harness/test-harness.git
    cd test-harness/
    cp -R ~/anthos-multicloud/anthos-multicloud-workshop/platform_admins/tests/test_harness/. .
    git add .
    git commit -m 'test'
    git branch -m master main
    git push origin main
    ```

    > If you are testing a specific branch, specify the appropriate `BRANCH` in the push command.

    ```bash
    git push origin main -o ci.variable="BRANCH=main"
    ```

### Observing the pipeline

1. Check out the status of the pipeline using the link below. Based on the current setup of the pipeline, it can take upwards of 45-60 minutes to complete due to dependencies.

    ```bash
    echo -e "https://gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog/test-harness/test-harness/-/pipelines"
    ```

    <img alt="Image" src="img/test-harness-pipeline.png" width=70% height=70%>

    > If the pipeline fails at any point in time, checkout the job details and retry as needed. Sometimes you may need to retry the actual pipeline of the resource (i.e. redis, cockroachdb, bank-of-anthos etc)

    ```bash
    echo -e "https://gitlab.endpoints.${GOOGLE_PROJECT}.cloud.goog/dashboard/projects"
    ```
