# AMCW: Multicloud Online Boutique

## Objectives

1. Deploy a microservices based application in all environments split across multiple clusters.
1. Manually migrating a Service from a cluster in AWS (EKS) to a cluster in GCP (GKE)
1. Deploy a [Distributed Service](https://medium.com/google-cloud/gke-multi-cluster-life-cycle-management-series-b0d39080ac6b) to multiple clusters in multiple cloud environments.

## Setup

- In the GCP console, open [Cloud Shell](https://shell.cloud.google.com/). This lab is intended to be run from Cloud Shell.

- Wait for the `vars.sh` file to be available.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      gsutil ls gs://${DEVSHELL_PROJECT_ID}/vars.sh &> /dev/null
      exit_code=$?
      
      clear

      if [ ${exit_code} -eq 0 ]; then
          echo "${TIMESTAMP} vars.sh file is available"  
          break
      fi
      
      echo "${TIMESTAMP} vars.sh file is not ready yet..."  
      sleep 5
  done
  ```

- Create a `WORKDIR` for this tutorial. All files related to this tutorial end up in `WORKDIR`.

  ```bash
  mkdir -p $HOME/anthos-multicloud && cd $HOME/anthos-multicloud && export WORKDIR=$HOME/anthos-multicloud
  ```

- Clone the workshop repo.

  ```bash
  git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop.git ${WORKDIR}/anthos-multicloud-workshop
  ```

- Download the `var.sh` file,

  ```bash
  gsutil cp gs://${DEVSHELL_PROJECT_ID}/vars.sh ${WORKDIR}/
  ```

- Source the `vars.sh` file.

  ```bash
  source ${WORKDIR}/vars.sh
  ```

- Install the required tools.

  ```bash
  mkdir -p ~/.cloudshell && touch ~/.cloudshell/no-apt-get-warning
  ${WORKDIR}/anthos-multicloud-workshop/platform_admins/scripts/tools.sh
  ```

## User setup

- Check that all of the builds have completed successfully.

  ```bash
  while true; do
      TIMESTAMP=`date +"[%Y-%m-%d_%H:%M:%S]"`
      ONGOING_BUILDS=`gcloud builds list --ongoing 2> /dev/null`
      ONGOING_BUILDS_COUNT=$(echo -n "${ONGOING_BUILDS}" | wc -l)

      clear

      if [ "${ONGOING_BUILDS_COUNT}" -eq "0" ]; then
          echo "${TIMESTAMP} All builds have completed, check the status of the builds at https://console.cloud.google.com/cloud-build/builds"
          break
      fi

      echo -en "${TIMESTAMP} ${ONGOING_BUILDS_COUNT} build(s) still running, build progress can also be monitored at https://console.cloud.google.com/cloud-build/builds\n${ONGOING_BUILDS}\n"
      sleep 5
  done
  ```

  <img alt="Image" src="img/cloudbuild_success.png" width=70% height=70%>

- Run the `user_setup.sh` script.

  ```bash
  source ${HOME}/.bashrc
  source ${WORKDIR}/anthos-multicloud-workshop/user_setup.sh
  ```

- This `user_setup.sh` script performs the following steps:

  - Downloads EKS cluster _kubeconfig_ files. The location of these files is in the `${WORKDIR}/kubeconfig` folder.
  - Downloads SSH-Key pair. SSH Keys are used to interact with Gitlab repos. The location of these files is in the `${WORKDIR}/ssh-keys` folder.
  - Downloads the Gitlab hostname and root password txt file. The location of the file is in the `${WORKDIR}/gitlab` folder.
  - Creates a combined _kubeconfig_ file with all cluster contexts. Renames the clusters for easy context switching. The location of the merged _kubeconfig_ file is `${WORKDIR}/kubeconfig/workshop-config`. The script also sets this as your `KUBECONFIG` variable.
  - Get the EKS cluster's Kubernetes Service Account tokens to login to through the Cloud Console. Learn about logging in to Anthos registered clusters [here](https://cloud.google.com/anthos/multicluster-management/console/logging-in).
    > The script is idempotent and can be run multiple times.

  Example output from the end of the `user_setup.sh` script:

  ```bash output
  *** eks-prod-us-west2ab-1 Token ***

  [EKS Cluster Token]

  *** eks-prod-us-west2ab-2 Token ***

  [EKS Cluster Token]

  *** eks-stage-us-east1ab-1 Token ***

  [EKS Cluster Token]

  *** Gitlab Hostname and root password ***

  gitlab.endpoints.PROJECT_ID.cloud.goog
  [`root` PASSWORD]
  ```

### Variables

The `user_setup.sh` script adds additional variables for the workshop to the `vars.sh` file. The `vars.sh` file is automatically sourced when you log in to Cloud Shell. You can also manually source the file.

### Logging in to EKS clusters

There are three EKS clusters in the architecture. Two clusters in the `prod` environment and one in the `stage` environment. The tokens from the `user_setup.sh` script can be used to log in to the EKS clusters in Cloud Console.

- Navigate to the **Kubernetes Engine > Clusters** page in Cloud Console. You can see the three EKS clusters registered. They have not been logged in.

  <img alt="Image" src="img/eks_loggedout.png">

- Click **Login** next to each EKS cluster and select **Token**. Copy and paste the tokens (outputted from the `user_setup.sh` script) to the EKS clusters.
- Note: Simply select the token in the cloud shell (and nothing else, this automatically copies is), and paste it into the token box in the Cloud Console without intermittently storing it into a file. Any intermittent storage might add extra characters that will invalidate the token and login will fail.

  <img alt="Image" src="img/eks_login.png" width=40% height=40%>

- Navigate to the **Workloads** and **Services** pages and verify you can see metadata information from the EKS clusters. This confirms you have successfully logged in.

  <img alt="Image" src="img/eks_workloads.png" width=80% height=80%>

## Deploying Online Boutique application

1. Deploy the [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo) sample application in all environments by running the following scripts.

   **Development**

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob_dev.sh
   ```

   **Staging**

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob_stage.sh
   ```

   **Production**

   ```bash
   ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob_prod.sh
   ```

   Online Boutique is a microservices based application composed of 11 microservices.
   These script deploy the Online Boutique application in all environments with microservices split across all clusters within each environment.
   Services are deployed on all clusters. Deployments are only created in one cluster within an environment for every microservice.
   The script ensures all Deployments are _Ready_ before exiting.

   Example output of the end of the deployment script:

   ```bash output
   *** Access Online Boutique app in namespace ob-prod by navigating to the following address: ***

   http://35.34.33.32
   ```

   You can access Online Boutique application through the `frontend` Service. The `frontend` Service is exposed via the `istio-ingressgateway` Service using a TCP L4 load balancer.
   The public IP address of the `istio-ingressgateway` Service is outputted by each script.

2. Copy and paste the public IP address of the `istio-ingressgateway` Service for each environment into a browser tab.
3. You should see the homepage of the Online Boutique application.

   <img alt="Image" src="img/ob-frontend.png" width=70% height=70%>

4. Navigate around the application to ensure complete functionality. You should be able to browse items, place them in cart, add additional items to the cart and checkout.
5. Inspect the **Workloads** for each environment through Cloud Console. Navigate to the **Kubernetes Engine > Workloads** page in Cloud Console.

   <img alt="Image" src="img/gke-workloads-menu.png" width=30% height=30%>

6. Online Boutique application is installed in different namespaces for different environments. The namespaces are names `ob-<env>`. For example, in `prod` environment, Online Boutique is installed in the namespace `ob-prod`. From the **Namespace** dropdown, select the `ob-prod` namespace and click **OK**.

   <img alt="Image" src="img/gke-namespace-dropdown.png" width=20% height=20%>

7. You can now see Deployments for all clusters (GKE and EKS) in the `prod` environment. Click `Cluster` in the table to sort by cluster. You can see that every Deployment is running on one (of four) clusters.

   <img alt="Image" src="img/gke-workloads-ob-prod.png" width=70% height=70%>

For Online Boutique to function, all microservices must be _Ready_ and be able to communicate across to other Services. See [Online Boutique Architecture](https://github.com/GoogleCloudPlatform/microservices-demo#service-architecture) for details on service to service connectivity. Anthos Service Mesh ([ASM](https://cloud.google.com/anthos/service-mesh)) allows you to create multi-cloud service mesh between Anthos and Anthos attached clusters running anywhere. In this case, ASM creates a multi-cloud service mesh between GKE and EKS clusters.

Anthos Service Mesh is designed to be _ambient_, meaning it operates transparently at the platform layer without interfering with the Services. This allows you to simply move a workload (a Deployment) from one cluster to another even across clouds wihtout any additional configuration.

## Manually migrating a service from AWS to GCP

With the Anthos platform, you can move the workloads (i.e. Kubernetes Deployments) from one cluster to another. There are multiple reasons you may want to do this.

- Migrating workloads from one environment to another while still being connected to services in the original environment.
- Moving workloads to a different tier. For example, you may want to move a workload from a cluster with standard instances to one with preemptible instances.

> The steps below show a manual process of moving a workload. In production, you would use an orchestrated process (and a pipeline) to perform a migration. This section is for educational purposes only.

Move `cartservice` Deployment from an EKS cluster to a GKE cluster in `prod` environment. You can use [Kiali](https://kiali.io/) to view the topology and realtime service graph as you migrate the Deployment.

1. Enable the `kiali` dashboard in the `${GKE_PROD_1}` production cluster.

   ```bash
   kubectl ctx ${GKE_PROD_1}
   kubectl ns istio-system
   istioctl dashboard kiali &
   ```

2. Click on the link in the output of the previous command.

   ```bash output
   Failed to open browser; open http://localhost:37351/kiali in your browser.
   ```

3. Click **Graph** from the left navigation menu.

4. Click on the **Select Namespaces** dropdown near the upper left and select the `ob-prod` namespace.

   <img alt="Image" src="img/kiali-ns-dropdown.png" width=50% height=50%>

5. Click on the **Versioned app graph** dropdown near the upper left and select **Service graph**.

   <img alt="Image" src="img/kiali-svc-dropdown.png" width=50% height=50%>

6. You can change the layout of the graph by clicking on the layout icons next to the **LEGEND** button near the lower left of the graph.

7. The lines represent connections between services. The color green represent `HTTP 200` between services. You can see everything is green and the application is working.

8. Click on the **Display** dropdown near the upper left and enable `Traffic Animation`. You can now see traffic flowing through the various services.

9. Delete `cartservice` deployment from the `${EKS_PROD_2}` cluster.

   ```bash
   kubectl ctx ${EKS_PROD_2}
   kubectl ns ob-prod
   kubectl delete deployment cartservice
   ```

10. Access the Online Boutique app by navigating to the `istio-ingressgateway` IP address of the `${GKE_PROD_1}` cluster.

   ```bash
   echo -n "http://"; kubectl --context ${GKE_PROD_1} get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].ip'
   ```

11. You get an `HTTP 500` error. The description states `could not retrieve cart`.

   <img alt="Image" src="img/cart-rm-eks2.png" width=50% height=50%>

12. After a few moments, switch to the Kiali tab and you can see errors showing for the `cartservice` in the service topology graph.

   <img alt="Image" src="img/kiali-cart-error.png" width=50% height=50%>

13. Deploy `cartservice` to `${GKE_PROD_2}` cluster. Note that you may pick any of the other three clusters.

   ```bash
   kubectl ctx ${GKE_PROD_2}
   kubectl ns ob-prod
   kubectl apply -f ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob/cart-deployment.yaml
   ```

14. Wait for the `cartservice` deployment to be available.

   ```bash
   kubectl wait --for=condition=available deployment/cartservice
   ```

15. Access the Online Boutique application again. It should be functional.

16. After a few moments, you can validate functionality in Kiali. The service topology graph should be green.

17. You can terminate Kiali by running the following command.

   ```bash
   killall istioctl
   ```

You have successfully migrated a service from an EKS cluster to a GKE cluster. You can perform the same steps to migrate any workload running in any cluster to any other cluster.

## Deploying a distributed service on GCP and AWS

A [Distributed Service](https://medium.com/google-cloud/gke-multi-cluster-life-cycle-management-series-b0d39080ac6b) is a stateless Kubernetes Service running on multiple Kubernetes clusters. A distributed service can run in multiple clusters in a single cloud. A distributed service can also run in multiple clusters in multiple cloud environments. Running a service in multiple clusters allows you to decouple the service lifecycle from the cluster lifecycle.

> The steps below show a manual process of deploying a distributed service. In production, you would use an orchestrated process (and a pipeline) to deploy a distributed service. This section is for educational purposes only.

Deploy `frontend` distributed service to GKE and an EKS cluster. Currently `frontend` is deployed only on `${GKE_PROD_1}` cluster, you add the `frontend` service to the `${EKS_PROD_1}` cluster.

1. Access the Online Boutique application by navigating to the `istio-ingressgateway` load balancer public IP address in `${GKE_PROD_1}` cluster.

   ```bash
   INGRESS_IP=$(kubectl --context ${GKE_PROD_1} get -n istio-system service istio-ingressgateway -o json | jq -r '.status.loadBalancer.ingress[0].ip')
   echo "http://$INGRESS_IP"
   ```

   The `frontend` Deployment is currently running on the `${GKE_PROD_1}` cluster. Navigate through the application by browsing items, adding them to cart, and checking out. You see a **Google Cloud** banner on every page. The banner represents the location of the `frontend` Deployment.

1. Deploy `frontend` to the `${EKS_PROD_1}` cluster.

   ```bash
   kubectl ctx ${EKS_PROD_1}
   kubectl ns ob-prod
   kubectl apply -f ${WORKDIR}/anthos-multicloud-workshop/platform_admins/tests/ob/frontend-deployment-aws.yaml
   ```

1. Wait for the `frontend` deployment to be available.

   ```bash
   kubectl wait --for=condition=available deployment/frontend
   ```

1. Access the Online Boutique application again. This time as you navigate through various pages of the application, you can see the banner on the page switching from **Google Cloud** to **AWS**. The **AWS** banner represents the `frontend` running in the `${EKS_PROD_1}` cluster.

   <img alt="Image" src="img/ob-aws-banner.png" width=70% height=70%>

   > Note: It may take a few minutes for the AWS banner to show up.

   `frontend` Service is now deployed to both GKE and EKS cluster. The incoming traffic is being load balanced between the two workload instances (Pods running in GKE and EKS cluster).

   [Anthos Service Mesh](https://cloud.google.com/anthos/service-mesh) allows you to connect services running on multiple Kubernetes clusters. The [ASM controlplane](https://istio.io/latest/docs/ops/deployment/architecture/#istiod) runs in every cluster and automatically discovers (and updates) Pod IP addresses for all services running inside the [service mesh](https://cloud.google.com/blog/products/networking/welcome-to-the-service-mesh-era-introducing-a-new-istio-blog-post-series). When you add (or remove) a Deployment to any cluster, the ASM controlplanes in every cluster discovers the change and updates all of the [Envoy proxy sidecars](https://istio.io/latest/docs/ops/deployment/architecture/#envoy) running in every Pod in the service mesh.

   You can view the Envoy proxy-configs for any Pod running in the mesh. You can view the IP addresses of endpoints for all services in the Envoy proxy-config.

1. In the `istio-ingressgateway` Pod in `${GKE_PROD_1}` running in the `istio-system` namespace, validate the `frontend` service has two endpoint IP addresses.

   ```bash
   export GKE_1_INGRESSGATEWAY_POD=$(kubectl get pod -n istio-system -l istio=ingressgateway --context=${GKE_PROD_1} -o jsonpath='{.items[0].metadata.name}')
   istioctl --context ${GKE_PROD_1} -n istio-system proxy-config endpoints ${GKE_1_INGRESSGATEWAY_POD} | grep "outbound|80||frontend.ob-prod.svc.cluster.local"
   ```

   Example output

   ```bash output
   10.0.1.9:8080                    HEALTHY     OK                outbound|80||frontend.ob-prod.svc.cluster.local
   44.241.102.240:443               HEALTHY     OK                outbound|80||frontend.ob-prod.svc.cluster.local
   ```

   The `istio-ingressgateway` Pod sees two endpoint IP addresses for the `frontend` distributed service. The 10.x.x.x IP address represents the `frontend` Pod running the GKE cluster. The other IP address is a public IP address. This is the public IP address of the `istio-ingressgateway` of the EKS cluster the other `frontend` Pod is running in (in this case `${EKS_PROD_1}` cluster).

   You can verify this by doing a `nslookup` on the `istio-ingressgateway` ELB hostname.

1. Validate the IP address of the `istio-ingressgateway` service running in the `${EKS_PROD_1}` cluster.

   ```bash
   export EKS_1_INGRESSGATEWAY_HOSTNAME=$(kubectl get svc -n istio-system -l istio=ingressgateway --context=${EKS_PROD_1} -o jsonpath='{.items[0].status.loadBalancer.ingress[0].hostname}')
   nslookup ${EKS_1_INGRESSGATEWAY_HOSTNAME}
   ```

   Example output

   ```bash output
   Server:         169.254.169.254
   Address:        169.254.169.254#53

   Non-authoritative answer:
   Name:   a741b013929f24ea09b4cfc7c761ef0d-22ffc5852748e8c0.elb.us-west-2.amazonaws.com
   Address: 54.68.78.179
   Name:   a741b013929f24ea09b4cfc7c761ef0d-22ffc5852748e8c0.elb.us-west-2.amazonaws.com
   Address: **44.241.102.240**
   ```

   You see that one of the IP addresses matches the output of the previous command.

   You have successfully deployed a distributed service to an EKS and GKE cluster.

## Traffic flow

![Image](img/traffic_flow.png)
