# Qwiklabs Prewarming for Hot Labs

## deployment_manager

This folder containers the Cloud Deployment Manager templates used to prewarm the lab environment.

## instance

This folder contains the scripts that apply the prewarming logic for the lab environment.
