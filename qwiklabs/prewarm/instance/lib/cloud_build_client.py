import google.auth

from google.cloud.devtools import cloudbuild_v1

class CloudBuildClient:
    client = None
    credentials = None
    project_id = None

    def __init__(self):
        self._initialize_client()

    def get_build_trigger(self, trigger_id):
        return self.client.get_build_trigger(project_id=self.project_id, trigger_id=trigger_id)

    def list_builds_by_tags(self, tags):   
        return [build for page in self.client.list_builds(filter=f"tags={tags}", project_id=self.project_id).pages for build in page.builds]

    def monitor_build(self, tags, retries=0):
        build_successful = False

        build = self.list_builds_by_tags(tags=tags)
        if build:
            build_successful = build[0].status == 3
            if not build_successful and build[0].status in [4, 6] and retries:
                self.client.retry_build(id=build[0].id, project_id=self.project_id)
                retries -= 1
                print(f"Retrying failed {tags} build, {retries} attempt(s) remaining", flush=True)
            elif not retries:
                print(f"{tags} build failed, no retries remaining", flush=True)
                raise Exception(f"{tags} build failed with no retries remaining")
        else:
            print(f"No {tags} build found", flush=True)

        return build_successful, retries
    
    def run_build_trigger(self, source, trigger_id):
        return self.client.run_build_trigger(project_id=self.project_id, source=source, trigger_id=trigger_id)

    def _initialize_client(self):
        self.credentials, self.project_id = google.auth.default()
        self.client = cloudbuild_v1.services.cloud_build.CloudBuildClient()