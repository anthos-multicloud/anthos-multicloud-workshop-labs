#!/usr/bin/env python

import os

from time import sleep

from lib.cloud_build_client import CloudBuildClient
from lib.execute_command import ExecuteCommand


def main():
    workshop_repo = os.getenv('WORKSHOP_REPO', 'https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop.git')
    workshop_branch = os.getenv('WORKSHOP_BRANCH', 'main')

    command = f"mkdir -p $WORKDIR && git clone --branch {workshop_branch} {workshop_repo} $WORKDIR/anthos-multicloud-workshop"
    execution = ExecuteCommand(command)

    return_code = execution.result.returncode
    std_err = "\n".join(execution.result.stderr)
    std_out = "\n".join(execution.result.stdout)

    if execution.result.returncode != 0:
        exception_message = "Execution failed!"
        print(f"\treturncode: {return_code}")
        print(f"\tstdout: {std_out}")
        print(f"\tstderr: {std_err}")

        raise Exception(exception_message)

    print(f"Successfully cloned repo\n\nEXECUTION RESULTS:\nReturn Code: {return_code}\nSTDOUT:\n{std_out}\nSTDERR:\n{std_err}")

    command = "cd $WORKDIR/anthos-multicloud-workshop && ./build.sh"
    execution = ExecuteCommand(command)

    return_code = execution.result.returncode
    std_err = "\n".join(execution.result.stderr)
    std_out = "\n".join(execution.result.stdout)

    if execution.result.returncode != 0:
        exception_message = "Execution failed!"
        print(f"\treturncode: {return_code}")
        print(f"\tstdout: {std_out}")
        print(f"\tstderr: {std_err}")

        raise Exception(exception_message)

    print(f"Successfully executed build.sh\n\nEXECUTION RESULTS:\nReturn Code: {return_code}\nSTDOUT:\n{std_out}\nSTDERR:\n{std_err}")

    print("Monitoring builds", flush=True)


    cloud_build_client = CloudBuildClient()

    trigger = cloud_build_client.get_build_trigger(trigger_id="push-to-main")
    
    build_triggered = False
    main_build = None
    for _ in range(3):
        main_build = cloud_build_client.list_builds_by_tags(tags=f"trigger-{trigger.id}")
        if main_build :
            build_triggered = True
            break
        sleep(10)

    if not build_triggered:
        print("Triggered main build not detect, manually triggering main build", flush=True)
        main_build = cloud_build_client.run_build_trigger(source=trigger.trigger_template, trigger_id=trigger.id)
    else:
        print("main build triggered on push", flush=True)

    main_build_successful = False
    while True:
        if main_build:
            main_build_successful = main_build[0].status == 3
            if main_build_successful:
                print("main build completed successfully", flush=True)
                break
            elif not main_build_successful and main_build[0].status == 4:
                print("main build failed", flush=True)
                raise Exception("main build failed")

        print("Waiting for main build to complete successfully", flush=True)
        sleep(30)
        main_build = cloud_build_client.list_builds_by_tags(tags="main")


    setup_build_successful = False
    while True:
        setup_build = cloud_build_client.list_builds_by_tags(tags="project-setup")

        if setup_build:
            setup_build_successful = setup_build[0].status == 3
            if setup_build_successful:
                print("project-setup build completed successfully", flush=True)
                break
            elif not setup_build_successful and setup_build[0].status == 4:
                print("project-setup build failed", flush=True)
                raise Exception("project-setup build failed")

        print("Waiting for project-setup build to complete successfully", flush=True)
        sleep(30)

    prod_build_successful = False
    stage_build_successful = False
    dev_build_successful = False
    gitlab_build_successful = False
    prod_build_retries = 2
    stage_build_retries = 2
    dev_build_retries = 2
    gitlab_build_retries = 2
    while True:
        prod_build_successful, prod_build_retries = cloud_build_client.monitor_build(tags="prod", retries=prod_build_retries)
        stage_build_successful, stage_build_retries = cloud_build_client.monitor_build(tags="stage", retries=stage_build_retries)
        dev_build_successful, dev_build_retries = cloud_build_client.monitor_build(tags="dev", retries=dev_build_retries)
        gitlab_build_successful, gitlab_build_retries = cloud_build_client.monitor_build(tags="gitlab", retries=gitlab_build_retries)

        if prod_build_successful and stage_build_successful and dev_build_successful and gitlab_build_successful:
            print("All builds completed successfully", flush=True)
            break

        print("Waiting for environment builds to complete successfully", flush=True)
        sleep(30)

    print("build_lab.py completed")

if __name__ == "__main__":
    main()
