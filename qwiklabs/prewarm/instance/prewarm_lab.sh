#!/bin/bash -x 
SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

export GOOGLE_PROJECT=${GCP_PROJECT_ID}
export GCLOUD_USER=${GCP_PROJECT_ID}@${GCP_PROJECT_ID}.iam.gserviceaccount.com

export HOME=/root
export WORKDIR=${HOME}/anthos-multicloud

source /snap/google-cloud-sdk/current/completion.bash.inc
source /snap/google-cloud-sdk/current/path.bash.inc 
          
if [ ${PREWARM_TYPE} == "VARS_ONLY" ]; then
  echo "Prepare initial vars.sh file"
  gsutil mb -p ${GOOGLE_PROJECT} gs://${GOOGLE_PROJECT}

  mkdir -p ${WORKDIR}
  cat <<EOF > ${WORKDIR}/vars.sh
export GOOGLE_PROJECT=${GCP_PROJECT_ID}
export GCLOUD_USER=${GCP_USERNAME}
export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
EOF

  gsutil cp ${WORKDIR}/vars.sh gs://$GOOGLE_PROJECT/vars.sh
fi

build_lab_exit_code=0
if [ ${PREWARM_TYPE} == "FULL" ]; then
  apt-get --quiet update
  apt-get --quiet install -y jq python-is-python3 python3-pip
  pip install --quiet -r ${SCRIPT_PATH}/requirements.txt

  ${SCRIPT_PATH}/build_lab.py
  build_lab_exit_code=$?

  echo "Prepare vars.sh file for the student"
  cp ${WORKDIR}/vars.sh ${WORKDIR}/vars.sh.student
  sed "s|GCLOUD_USER=${GCLOUD_USER}|GCLOUD_USER=${GCP_USERNAME}|" -i ${WORKDIR}/vars.sh.student
  sed "s|WORKDIR=${HOME}|WORKDIR=${GCP_USER_HOME}|" -i ${WORKDIR}/vars.sh.student
  gsutil cp ${WORKDIR}/vars.sh.student gs://$GOOGLE_PROJECT/vars.sh
fi

if [ "${build_lab_exit_code}" -eq 0 ]; then
  gcloud beta runtime-config configs variables set \
    success/qwiklabs-prewarm-instance-waiter \
    success --config-name prewarm-runtime-config
else
  gcloud beta runtime-config configs variables set \
    failure/qwiklabs-prewarm-instance-waiter \
    failure --config-name prewarm-runtime-config
fi

gcloud compute instances delete qwiklabs-prewarm-instance --quiet --zone=${GCP_ZONE}
