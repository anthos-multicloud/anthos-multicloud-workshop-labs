# Converter

## generate_mermaid_images.sh

This script generates SVGs for all of the .mmd files in each lab's `diagrams` directory and put them in the lab's `img` directory. Any existing SVG files are overwritten on each execution. It requires `docker` in order to run the [containerized](https://hub.docker.com/r/minlag/mermaid-cli) version of the [`mermaid-cli`](https://github.com/mermaid-js/mermaid-cli).

Usage:

``` bash
git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop-labs.git
cd anthos-multicloud-workshop-labs/qwiklabs/converter
./generate_mermaid_images.sh
```

## generate_qwiklabs_assets.sh

This script puts the labs into the format that the Qwiklabs repo([CloudVLab/gcp-ce-content](https://github.com/CloudVLab/gcp-ce-content.git)) expects and then moves them into the repo directory.

Since the [CloudVLab/gcp-ce-content](https://github.com/CloudVLab/gcp-ce-content.git) repository is so large you can specify an existing repository path, via the `OUTPUT_DIR` environment variable, if there is an existing clone of the repo on your host.

``` bash
git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop-labs.git
cd anthos-multicloud-workshop-labs/qwiklabs/converter
OUTPUT_DIR=<path to CloudVLab/gcp-ce-content repo> ./generate_qwiklabs_assets
```

Otherwise, it is possible to run the following command which will clone the [CloudVLab/gcp-ce-content](https://github.com/CloudVLab/gcp-ce-content.git) repo into the `qwiklabs/converter/qwiklabs/gcp-ce-content` directory.

```bash
git clone https://gitlab.com/anthos-multicloud/anthos-multicloud-workshop-labs.git
cd anthos-multicloud-workshop-labs/qwiklabs/converter
./generate_qwiklabs_assets
```


```
