#!/bin/bash

SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
REPO_ROOT=$(realpath ${SCRIPT_PATH}/../..)

echo ${REPO_ROOT}

STAGING_DIR=${SCRIPT_PATH}/.staging/mermaid
rm -rf  ${STAGING_DIR}
mkdir -p ${STAGING_DIR}

echo "Pulling minlag/mermaid-cli image"
docker pull minlag/mermaid-cli
echo

LABS=$(find ${REPO_ROOT} -maxdepth 1 -type d -regex ".*/lab_[0-9]+" | sort)
for lab in ${LABS}; do
    lab_name=$(basename ${lab})
    echo "Processing diagrams for ${lab_name}(${lab})"
    LAB_FOLDER=${lab}
    DIAGRAM_FOLDER=${LAB_FOLDER}/diagrams

    diagrams=$(find ${DIAGRAM_FOLDER} -maxdepth 1 \( -type l -o -type f \)  -regex ".*\.mmd")
    for diagram_file in ${diagrams}; do
        diagram_name=$(basename ${diagram_file} .mmd)
        echo "Processing diagram ${diagram_name}(${diagram_file})"
        docker run -it \
          --user "$(id -u):$(id -g)" \
          -v ${SCRIPT_PATH}/mermaid-conf:/config \
          -v ${LAB_FOLDER}:/data \
          -v ${REPO_ROOT}/lab_shared:/lab_shared \
          -v ${STAGING_DIR}:/staging \
          minlag/mermaid-cli \
          --backgroundColor transparent \
          --cssFile /config/custom.css \
          --input /data/diagrams/${diagram_name}.mmd \
          --output /staging/${diagram_name}.png \
          --scale 3
    done

    mv -f ${STAGING_DIR}/*.png ${LAB_FOLDER}/img/ 2>/dev/null
    echo
done

return_code=0
if git diff --exit-code --no-patch; then
    echo "No changes detected"
else
    echo "***** Changes detected, please commit the changes *****"
    echo
    git status
    return_code=1
fi

exit ${return_code}
