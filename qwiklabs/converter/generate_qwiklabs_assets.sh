#!/bin/bash

SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
REPO_ROOT=$(realpath ${SCRIPT_PATH}/../..)

declare -A LAB_MAPPING
LAB_MAPPING=(
    ["lab_00"]="ce010-anthos-multicloud-workshop"
    ["lab_01"]="ce012-anthos-multicloud-workshop-lab1"
    ["lab_02"]="ce013-anthos-multicloud-workshop-lab2"
    ["lab_03"]="ce014-anthos-multicloud-workshop-lab3"
    ["lab_04"]="ce015-anthos-multicloud-workshop-lab4"
    ["lab_05"]="ce016-anthos-multicloud-workshop-lab5"
    ["lab_06"]="ce020-anthos-multicloud-workshop-lab6"
    ["lab_07"]="ce021-anthos-multicloud-workshop-lab7"
)

STAGING_DIR=${SCRIPT_PATH}/.staging/qwiklabs
rm -rf  ${STAGING_DIR}
mkdir -p ${STAGING_DIR}

OUTPUT_DIR=${OUTPUT_DIR:-${SCRIPT_PATH}/gcp-ce-content}
mkdir -p ${OUTPUT_DIR}

cd ${OUTPUT_DIR}
if [ ! -d "${OUTPUT_DIR}/.git" ]; then
    git clone https://github.com/CloudVLab/gcp-ce-content.git ${OUTPUT_DIR}/
fi

echo "git pull on ${OUTPUT_DIR}"
git -C ${OUTPUT_DIR} pull

LABS=$(find ${REPO_ROOT} -maxdepth 1 -type d -regex ".*/lab_[0-9]+" | sort)
for lab in ${LABS}; do
    lab_name=$(basename ${lab})
    echo "Processing lab ${lab_name}(${lab})"
    LAB_FOLDER=${lab}
    LAB_STAGING_DIR=${STAGING_DIR}/${lab_name}
    QWIKLAB_FOLDER=${LAB_MAPPING[${lab_name}]}
    DM_FOLDER=${REPO_ROOT}/qwiklabs/prewarm/deployment_manager

    mkdir -p ${LAB_STAGING_DIR}/instructions
    cp -Lpr ${LAB_FOLDER}/* ${LAB_STAGING_DIR}/instructions/
    rm -rf ${LAB_STAGING_DIR}/instructions/diagrams
    mv ${LAB_STAGING_DIR}/instructions/README.md ${LAB_STAGING_DIR}/instructions/en.md 


    sed 's/```$/\<\/ql-code-block>/' -i ${LAB_STAGING_DIR}/instructions/en.md 
    sed 's/```\([[:alpha:]]*\)\(.*\)/\<ql-code-block language="\1"\2>/' -i ${LAB_STAGING_DIR}/instructions/en.md 
    
    mkdir -p ${LAB_STAGING_DIR}/deployment_manager
    cp -Lpr ${DM_FOLDER}/* ${LAB_STAGING_DIR}/deployment_manager/

    echo "Moving staged files to ${OUTPUT_DIR}/labs/${QWIKLAB_FOLDER}/"
    rm -rf ${OUTPUT_DIR}/labs/${QWIKLAB_FOLDER}/deployment_manager ${OUTPUT_DIR}/labs/${QWIKLAB_FOLDER}/instructions 
    mv ${LAB_STAGING_DIR}/* ${OUTPUT_DIR}/labs/${QWIKLAB_FOLDER}/

    echo
done

git -C ${OUTPUT_DIR} status
