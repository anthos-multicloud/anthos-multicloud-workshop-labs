# Anthos Multicloud Workshop(AMCW) Overview

## About the workshop

This Anthos Multicloud Workshop(AMCW) was built to learn, build and manage an Anthos platform in a multicloud environment. Instead of a technology (or product) focused approach, the workshop takes a persona-focused approach. Each lab in the workshop addresses a user journey or story. Three main personae interact with Anthos:

1. **Application Developers or Owners** - Application developers are primarily responsible for writing and debugging application code.
1. **Application or Service Operators** - Also sometimes affectionately known as [SRE](https://landing.google.com/sre/), are responsible for running applications/services, monitoring the health and status of live deployments, and rolling out updates.
1. **Platform Administrators** - Platform administrator are part of a centralized platform team responsible for creating and managing resources for application teams and managing the organization’s central infrastructure.

The labs in the workshops are designed with these user personae their user journeys in mind.

## Labs

1. **Building the foundation**
   - _I, platform administrator, want to build and manage an Anthos platform in a multicloud environment._
1. **Multicloud applications and distributed services**
   - _I, application owner and operator, want to deploy my applications across multiple clusters in multiple cloud environments._
   - _I, application operator, want to move/migrate services between clusters in a multicloud environment._
   - _I, application owner and operator, want to run the same service in multiple clusters in multiple cloud environments._
1. **Monitoring and service level objectives**
   - _I, application operator, want to use Cloud Monitoring to monitor metrics from all services running in all clusters in multiple cloud environments._
   - _I, application operator, want to set SLOs and alerts for services running in my clusters_
1. **Configuration and Policy**
   - _I, application operator, want to manage configurations and policies for all my clusters centrally_
1. **Adding new clusters**
   - _I, platform administrator, want to add a new cluster to my multicloud environment._
1. **Online Boutique CI/CD Pipeline**
   - _I, application owner and operator, want to deploy an application across multiple clusters in multiple cloud environments using a CI/CD pipeline._
1. **Bank of Anthos CI/CD Pipeline**
   - _I, application owner and operator, want to deploy an application across multiple clusters in multiple cloud environments using a CI/CD pipeline._

## Introduction to Anthos

[Anthos](https://cloud.google.com/anthos )is a hybrid and multicloud platform that allows enterprises to build, deliver, and manage modern application life cycles in a fast, scalable, reliable, and secure way. In addition to [modern applications](https://cloud.google.com/solutions/cloud-native-app-development), Anthos also integrates to existing applications and application infrastructure which allows enterprises to modernize in place, in the cloud and at their own pace. The Anthos platform is cloud-agnostic. It can run in an on-premises data center, GCP or any cloud environment. Anthos platform is composed of several components that provide the following functionality:

- Container management (via [Anthos GKE](https://cloud.google.com/anthos/gke) or [Anthos attached clusters](https://cloud.google.com/anthos/docs/setup/attached-clusters))
- Policy management and enforcement (via [Anthos Configuration Management](https://cloud.google.com/anthos/config-management))
- Services management (via [Anthos Service Mesh](https://cloud.google.com/anthos/service-mesh))
- Application and software life cycle management ([CI/CD](https://cloud.google.com/solutions/modern-ci-cd-with-anthos))
- Observability and platform management (via [Cloud Operations](https://cloud.google.com/products/operations))

## Architecture

![Image](img/architecture.png)
